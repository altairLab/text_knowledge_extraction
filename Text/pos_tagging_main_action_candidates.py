# -*- coding: utf-8 -*-
#--------------------------------------------------------------------#
import spacy
import os
#--------------------------------------------------------------------#
path = os.path.join(".","<sample_sentences>.txt")
file = open(path, 'r')
sentences = file.readlines()
use_synonyms = ['use', 'utilise', 'utilize', 'employ', 'exercise']
not_useful_verbs = ['be', 'can', 'must', 'have', 'prefer']
nominalized_verbs = ['placement', 'reflection', 'retraction', 'exposure', 'resection', 'mobilization', 'traction', 'administration', 'excision', 'identification', 'application']
nlp = spacy.load("en_core_web_sm")
#----------------------------------------------------------------------#
if __name__ == '__main__':
    #output = ""
    main_action_candidates = list()
    for i, s in enumerate(sentences):
        doc = nlp(s)
        temp_list = list()
        for token in doc:
            if str(token.pos_) == 'VERB':
                if str(token.tag_) in ['VB', 'VBP', 'VBD', 'VBN', 'VBZ']:
                    temp_list.append([i, token.text, token.lemma_, token.tag_])
            elif token.text in nominalized_verbs:
                temp_list.append([i, token.text, token.lemma_, token.tag_])

        main_action_candidates.append(temp_list)
        #output = output + str((token.text, token.lemma_, token.pos_, token.tag_, token.dep_,token.shape_, token.is_alpha, token.is_stop)) + "\n"    
    main_action_finals = list()
    for c in main_action_candidates:
        if len(c) > 1:
            temp_list = list()
            for c2 in c:
                if c2[2] not in use_synonyms and c2[2] not in not_useful_verbs:
                    temp_list.append(c2)
            main_action_finals.append(temp_list)
        else:
            main_action_finals.append(c)             
    #This list is finally filtered by the SRL module that select one or more main action(s).
    #For eact main action candidate we report the id of the sentence it refers to, the original
    #text, the corresponding lemma and pos tag
    with open(os.path.join("<output_main_action_candidates>.txt"), "w") as text_file:
        for c in main_action_finals:
            text_file.write(str(c) + '\n')

