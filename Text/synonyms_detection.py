# -*- coding: utf-8 -*-
#----------------------------------------------------------------------#
import spacy
import os
from nltk.corpus import wordnet  
#----------------------------------------------------------------------#
path = os.path.join(".","<sample_paragraph>.txt")
file = open(path, 'r')
sentences = file.readlines()
nlp = spacy.load("en_core_web_sm")
synonyms_in_text = dict()
#----------------------------------------------------------------------#
if __name__ == '__main__':
    list_of_verbs = list()
    for i, s in enumerate(sentences):
        doc = nlp(s)
        temp_list = list()
        for token in doc:
            if str(token.pos_) == 'VERB':
                list_of_verbs.append(token.lemma_)
    for v in list_of_verbs:
        for synset in wordnet.synsets(v):
            temp_syn = set()
            for lemma in synset.lemmas():
                name = str(lemma.name()).replace("_", " ")
                if name != v:
                    temp_syn.add(name)
            for ts in temp_syn:
                if ts in list_of_verbs:
                    synonyms_in_text[v] = ts
    #If two actions are synonyms, they are considered the same ASP primitive.
    #This list is however given to the logician who verifies the goodness of 
    # the simplification before writing ASP rules
    with open(os.path.join("<output_synonyms_in_text>.txt"), "w") as text_file:
        for k in synonyms_in_text.keys():
            text_file.write(k + "->" + synonyms_in_text[k] + "\n")
