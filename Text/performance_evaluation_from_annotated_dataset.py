# -*- coding: utf-8 -*-
#--------------------------------------------------------------------#
from numpy import genfromtxt
from sklearn.metrics import precision_score, recall_score, f1_score, accuracy_score
#--------------------------------------------------------------------#
evaluation_file = "<file_name>"
data = genfromtxt(evaluation_file, delimiter='@', usecols=[2,3], skip_header=True, dtype=None, encoding='utf-8')
gold = list()
pred = list()
for e in data:
    gold.append(e[0])
    pred.append(e[1])
precision = precision_score(gold, pred, pos_label='Y', average='weighted')
recall = recall_score(gold, pred, pos_label='Y', average='weighted')
f1 = f1_score(gold, pred, pos_label='Y', average='weighted')
accuracy = accuracy_score(gold, pred)
#--------------------------------------------------------------------#
print("PRECISION: ", precision)
print("RECALL: ", recall)
print("F1: ", f1)
print("Accuracy", accuracy)


