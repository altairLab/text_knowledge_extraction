# -*- coding: utf-8 -*-
#--------------------------------------------------------------------#
from spacy_conll import init_parser
from allennlp.predictors.predictor import Predictor
from spacy.language import Language
from spacy.tokens import Doc
import os
import progressbar
#----------------------------------------------------------------------#
path = os.path.join(".","<sample_sentences>.txt")
file = open(path, 'r')
sentences = file.readlines()
#----------------------------------------------------------------------#
@Language.factory("srl", default_config={
    "model_path": "https://storage.googleapis.com/allennlp-public-models/structured-prediction-srl-bert.2020.12.15.tar.gz"})
def create_srl_component(nlp: Language, name: str, model_path: str):
    return SRLComponent(nlp, model_path)


class SRLComponent:
    def __init__(self, nlp: Language, model_path: str):
        if not Doc.has_extension("srl"):
            Doc.set_extension("srl", default=None)
        self.predictor = Predictor.from_path(model_path)
    def __call__(self, doc: Doc):
        predictions = self.predictor.predict(sentence=doc.text)
        doc._.srl = predictions
        return doc
#----------------------------------------------------------------------#
if __name__ == '__main__':
    output = ""
    nlp = init_parser(parser="spacy", model_or_lang="en_core_web_sm", include_headers=False)
    nlp.add_pipe("srl")
    bar = progressbar.ProgressBar(maxval=len(sentences), widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
    bar.start()
    #Easy to read output
    for i, s in enumerate(sentences):
        bar.update(i)
        doc = nlp(s)
        output = output + "\n" 
        for j in range(0, len(doc._.srl["verbs"])):
            output = output + str(doc._.srl["verbs"][j]['description']) + "\n"
        output = output + "\n"
    bar.finish()
    # output then filtered using the main action candidate list. If a candidate is in a span of text
    # labeled as ADV, TMP, PRD, PRP or MNR by the NLU model, it is discarded from the list. Then, only
    # annotations relating to the main action(s) are kept.
    with open(os.path.join("<output_sample_sentences>.txt"), "w") as text_file:
        text_file.write(output)

