#ID@Sentence@Gold@Pred
1@Once the point 20 cm proximal is identified, the left hand continues to hold the bowel, while the right-hand needle driver is used to throw two stay sutures at the 20-cm mark, each just over 1 cm apart, with 3-0 silk.@Y@Y
2@We regularly start the procedure using the Si® system with monopolar cautery with hook tip (instrument 1), Maryland bipolar forceps (instrument 2), and grasping forceps (instrument 3).@Y@Y
3@We typically use the fenestrated bipolar cautery to accomplish ligation of the uterine vessels.@Y@Y
4@We typically cauterize sequentially with bipolar cautery and then partially transect the IP pedicle using a combination of the scissors and electrocautery with the monopolar scissors.@Y@Y
5@The uterine artery may then be transected lateral to the ureter, using bipolar and then monopolar cautery.@Y@Y
6@A small esophagotomy is made with electrocautery to allow the tube to be pulled into the peritoneal cavity.@Y@Y 
7@This dissection can be performed without cautery with the aid of clips to prevent thermal injury to adjacent structures.@N@Y
8@A wide enterotomy and spatulation on the ureter are necessary to pass the needle driver through the enterotomy in order to grab the stent.@N@Y
9@The ileum is then divided, using cautery at the proximal end of the conduit.@Y@Y
10@A needle driver is placed in the right medial port and the left port.@N@Y 
11@The hepatic flexure is taken down using cautery.@Y@Y
12@In general, the following instruments are used: monopolar scissors, PK dissecting forceps, large needle driver, and SutureCut needle driver.@N@Y 
13@Once the abdomen is entered, the needle driver holding the specimen bag is passed to the incision.@N@Y
14@The assistant uses a suction irrigator, needle driver, and assorted graspers.@Y@Y 
15@The lymphatic tissue is dissected off of the obturator vessels and nerve, paying close attention to minimize the use of monopolar cautery around the obturator nerve.@N@Y
16@Next, the plane funneling to the bladder neck between the base of the prostate and the bladder neck is developed with a combination of cautery and blunt dissection.@Y@Y
17@The remaining ileum for the neobladder is then detubularized using cautery along the antimesenteric edge.@Y@Y
18@Dissection is begun anteriorly through the bladder neck using cautery with a transverse incision that is deepened along the junction between bladder and prostate until the catheter is reached.@Y@Y
19@To begin, a grasping instrument (Cadiere or crocodile forceps) is attached to instrument arm 1, and a monopolar cautery hook or Maryland grasper is attached to instrument arm 2.@N@Y 
20@The medial umbilical ligaments and urachus are divided using cautery proximally along the anterior abdominal wall.@Y@Y
21@The lymphatic tissue is then mobilized in a split-and-roll technique off of the external iliac vessels, using a combination of cautery and clips.@Y@Y
22@The ileum is then divided using cautery at the proximal end of the afferent limb of the neobladder.@Y@Y
23@The peritoneum lateral to the medical umbilical ligaments is incised, and the space of Retzius is developed using blunt dissection and cautery.@Y@Y
24@Finally, a colpotomy incision can be created either with the monopolar scissor cautery or an instrument suitable to the surgeon.@Y@Y
25@The scissor is replaced with a needle driver; The assistant now brings in a 60-cm purple load for the Endo-GIA Tri-Staple™, or a 3.5-mm blue load, and both the arm 2 and arm 3 graspers are used on the bowel to fit the stapler blades into the defects that were created.@Y@Y
26@Importantly, meticulous hemostasis is achieved with bipolar cautery of the perforating vessels off of the psoas muscle and pelvic sidewall.@Y@Y
27@A hook electrocautery is attached to arm 4.@N@Y
28@The needle driver is passed through the ureteral anastomosis into the defect in the suture line; the stent is grasped, pulled through the afferent limb, and then passed into the ureter.@N@Y
29@In the medial right hand, the needle driver is exchanged with a scissor.@N@Y 
29@In the medial right hand, the needle driver is exchanged with a scissor.@N@Y
30@Distally, clips or cautery should be used to ligate lymphatic vessels stemming from the leg, for lymphostasis.@Y@Y
31@After the second load is fired, the needle driver is used to throw a secure “crotch” stitch at the end of the staple line.@Y@Y
32@The subcutaneous tissue is divided using cautery, and a cruciate incision is made in the anterior rectus fascia.@Y@Y
33@Tumor excision can be started using monopolar cautery through the renal cortex, followed by sharp excision around the tumor including a margin of normal tissue.@Y@Y
34@The incision is then started at the desired point on the vagina using monopolar cautery and extended circumferentially.@Y@Y
35@The colonic portion of the Indiana pouch is then detubularized using cautery along its antimesenteric edge.@Y@Y
36@The fenestrated bipolar cautery can be again used to first cauterize and then transect the tissues with the monopolar scissors.@Y@Y
37@In the lower port, the assistant passes a laparoscopic needle driver and secures the suture of the specimen bags.@N@Y
38@Throughout the dissection, cautery is utilized for meticulous hemostasis, and clips are used judiciously at both the proximal and distal limits to prevent lymphatic leakage.@Y@Y
39@Once adequately suture ligated, the complex can be divided sharply or with cautery.@Y@Y
40@Prior to hilar clamping, the planned margin of excision is marked on the surface of the renal capsule using cautery.@Y@Y
41@The lymph node packet is dissected away from the obturator nerve and vessels, with care taken to avoid the use of monopolar cautery in this location.@N@Y 
42@It is grabbed with the robotic needle driver, and the tip is placed into the left ureter, using the Cadiere grasper to aid the passage.@Y@Y
43@Once the tips are seen at the level of the anastomosis, the left-hand grasper is used to place the end of the stent into the jaws of the needle driver, which pulls it through the bowel.@Y@Y
44@The assistant grabs the end of the wire with a laparoscopic needle driver and passes it in through one of the assistant ports.@Y@Y
45@With elevation of the bladder and cranial retraction of the uterus, the vesicouterine space is easily developed with the use of monopolar cautery and blunt dissection.@Y@Y
46@The urethral aperture is then created sharply or with cautery using the cutting current.@Y@Y
47@The superficial dorsal vein can be controlled with bipolar cautery and the anterior prostate fat cleared off the surface of the prostate because it occasionally harbors lymph nodes.@Y@Y
48@The obturator vessels and nerve are prospectively identified, and the lymphatic tissue is dissected off with meticulous hemostatic and lymphatic control, using bipolar electrocautery and Hem-o-lok® clips.@Y@Y
49@The excess is then excised and discarded using cautery.@Y@Y
50@Once we are ready to clip the cystic duct or artery, we exchange the hook electrocautery for the medium-large clip applier.@N@Y
51@Bipolar cautery can be used to coagulate the IP ligament.@Y@Y
52@Once in location, the needle driver is used to pinch the stent next to the wire, not including the wire in the grasp, which will allow the assistant to pull the wire while leaving the stent in place.@Y@Y
53@The permanent cautery hook is then placed at the utility incision port (surgeon right hand: arm 1), and the fenestrated bipolar forceps is finally introduced operative port (surgeon right hand; arm 2).@N@Y
53@The permanent cautery hook is then placed at the utility incision port (surgeon right hand: arm 1), and the fenestrated bipolar forceps is finally introduced operative port (surgeon right hand; arm 2).@N@Y
54@Perforating vessels can be easily identified and ligated using bipolar cautery.@Y@Y
55@All lymphatic channels proximally should be sealed with either clips or cautery.@Y@Y
56@Care should be taken to ligate the superficial dorsal venous complex with bipolar cautery prior to its division.@Y@Y
57@The peritoneum is then incised, and the rectovaginal space is developed as caudally as is necessary to achieve an adequate vaginal margin; this is achieved using both blunt dissection and monopolar cautery.@Y@Y
58@The lymphatics distal to Cloquet’s node should be ligated using clips or cautery.@Y@Y