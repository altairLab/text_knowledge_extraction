#ID@Sentence@Gold@Pred
1@if the uterine manipulator that is being used does not have a pneumo-occluder, or if a uterine manipulator is not being used, a pneumo-occluder should be placed prior to incising the vagina.@Y@Y
2@if the ovaries and fallopian tubes are being spared, one must also first isolate the utero-ovarian ligament.@Y@Y
3@if pneumoperitoneum is not established through the umbilicus, a 10–12-mm trocar is then placed either directly above or below the umbilicus (depending on the patient’s habitus).@Y@Y
4@if a previous 6 o-clock posterior urethral stitch was used, it is placed in the corresponding 6 o-clock position of the bladder neck.@Y@Y
5@upper endoscopy is performed before positioning, to assess the position of the tumor and the extent of gastric cardia and fundus involvement, if any.@Y@N 
6@if the dissection is too deep into the lobe, it may lead to isolation and division of only a few branches rather than the whole vein.@Y@Y
7@if an oophorectomy is not planned, the ip ligament should be left intact and the utero-ovarian ligament and fallopian tube transected instead, in the same fashion.@Y@Y
8@if not performed initially but instead approached after bladder neck transection, the anterior layer of denonvilliers fascia covering the vas deferens and seminal vesicles is incised.@Y@N
9@if the left phrenic nerve can not be identified, the left pleural space is entered and the scope passed across the midline into the left chest.@Y@Y
10@if the contralateral pleural space was entered, a small chest tube is placed across the midline, aspirated, and removed.@Y@Y
11@a 24-fr foley catheter is brought into the abdomen through the right upper quadrant port site, or a separate 1-cm incision if the port site is too cranial.@Y@N
12@following fascial closure, the abdomen may be reinsufflated to ensure hemostasis and to allow for perioperative drain placement, if appropriate.@Y@Y
13@if an orthotopic neobladder diversion is planned, a 3-0 polyglactin suture should be placed in the 6 o-clock position in the posterior urethral stump prior to division of the posterior urethra.@Y@Y
14@an assistant's trocar may be placed laterally in the left lower quadrant if necessary.@Y@Y
15@if sln assessment is also being done, the lymphadenectomy should be performed first.@Y@Y
16@if an orthotopic continent diversion is planned , the endopelvic fascia should be spared to improve continence outcomes.@Y@Y
17@if using the xi system, the endoscopic camera is inserted into the umbilical trocar and attached to arm 3.@Y@Y
18@if an extra capsular mass was present without a preoperative pathologic diagnosis, a biopsy sample was obtained.@Y@Y
19@adjust the table if necessary to achieve the best view.@Y@Y
20@if slns are not to be assessed , the lymphadenectomy may be done either before or after the radical hysterectomy.@Y@Y
21@if needed , the ureter can be incised more proximally to increase the amount of spatulation.@Y@Y
22@if the take-off of the superior segment branch is too separated from the basal trunk and difficult to be isolated in one single step, the artery should be divided into two separated steps.@Y@Y
23@the assistant port should be a standard 10-mm trocar to allow the easy passage of needles if suturing.@Y@Y
24@upper pole dissection of hepatorenal ligaments can be useful if further renal mobilization is required to access the tumor or to plan for reconstruction following excision.@Y@Y
25@if using the double-docking technique, the robot should first be docked from the left, about 15 degrees from the left shoulder.@Y@Y
26@to remove the gallbladder, an endoscopic retrieval bag is inserted through any trocar (upsized if needed), and the specimen is collected.@Y@Y
27@conversion to laparotomy must be considered if hemostasis can not beachieved endoscopically.@Y@Y
28@if not, secure the ileum on either side of the previous suture until arm 3 can be removed.@Y@Y
29@vaginal probes, eea sizers (medtronic minimally invasive therapies, minneapolis, mn), or a sponge stick may be used to make the colpotomy if uterine manipulation is not felt to be necessary, or if a large cervical tumor prohibits placement of a manipulator.@Y@Y
30@if space does not allow for this configuration, the vision cart can be placed to the patient's upper right, close enough for all the connecting wires to reach without tension.@Y@Y
31@if the tumor is not appreciable, especially in the case of tumors at the gastroesophageal junction , endoscopy should be utilized to confirm its location.@Y@Y
32@further anterior dissection may be performed if needed after the hysterectomy with the aid of a vaginal manipulator.@Y@Y
33@lymph node dissection, if indicated, can also be accomplished using this instrument.@Y@Y
34@if a nerve-sparing procedure is planned, the pedicles should be stapled up to the tips of the seminal vesicles.@Y@N
35@for lower esophageal tumors, portions of the right or left crus may be dissected free and removed with the esophagus if cause for suspicion of tumor involvement is identified.@Y@Y
36@at this point, if a nerve-sparing procedure is performed, the prostatic fasciae are released high on the lateral prostate with sharp dissection.@Y@Y
37@additional cortical reapproximation sutures can be placed if necessary.@Y@Y
38@if the uterus is not to be removed, the utero-ovarian ligament is isolated, cauterized, and transected in order to completely remove the ovary and tube.@Y@Y
39@alternatively, the sutures can be placed after the excision takes place if the surgeon feels that the suture may get in the way while extirpating the lesion.@Y@Y
40@if there is significant tension on this initial 6 o-clock suture, additional interrupted sutures can be used to reapproximate the posterior urethral plate.@Y@Y
41@the ileum should stay in place if arm 3 is removed.@Y@Y
42@if a replaced left hepatic artery is encountered, it is clipped temporarily.@Y@Y
43@the umbilical fascia defect is closed with interrupted 0-vicryl suture, the umbilicus is tacked down to the fascia if necessary, and the skin closed with absorbable subcuticular suture.@Y@Y
44@if sln detection is planned, the cervical injection of dye should be done prior to placement of a uterine manipulator.@Y@Y
45@if nerve sparing is not planned, the pedicle to the prostate and neurovascular bundle can also be stapled, extending to the apex of the prostate ( fig. 14.5 ).@Y@Y
46@if necessary, however, the laparoscopic incisions can be closed, the tape removed, and the abdomen re-prepared for surgery (antiseptic wash and draping).@Y@Y
47@if this occurs, the surgeon considers a low threshold for the performance of tube thoracostomy.@Y@Y
48@following fascial closure, the abdomen may be reinsufflated to ensure hemostasis and to allow for perioperative drain placement, if appropriate.@Y@Y
49@the oblique fissure (right middle and lower lobe fissure) is often complete; if the fissure is incomplete, we divide it introducing the stapler from the utility incision after identifying the pulmonary artery.@Y@Y
50@if performed initially, an incision is made in the peritoneum at the rectovesical pouch a few centimeters above the visualized rectum.@Y@Y
51@if total omentectomy is required, the dissection of the greater omentum is more easily performed at the end of the dissection, prior to the reconstruction.@Y@Y
52@the fascia is closed at the port sites if the trocar is 10 mm or larger.@Y@Y
53@if a nerve-sparing procedure is not planned, the neurovascular bundle is divided using the laparoscopic stapler, and the endopelvic fascia is opened as described above.@Y@N
54@a temporary pelvic drain can then be inserted into the pelvis through one of the lateral port sites if desired.@Y@Y
55@if needed, an additional assistant's trocar can be inserted left of and lateral to the umbilicus, along the midclavicular line.@Y@Y
56@if more bowel is needed , as is necessary for a neobladder, the umbilical tape can be reset to allow more bowel to be isolated.@Y@Y
57@the entire mesentery can be divided up to the resection site and viability tested with the firefly technology if desired.@Y@Y
58@if using the si system, clear a path for the patient cart to approach the patient over the right shoulder (at 45°).@Y@N



