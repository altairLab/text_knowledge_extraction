# -*- coding: utf-8 -*-
#--------------------------------------------------------------------#
import os
from numpy import genfromtxt
#----------------------------------------------------------------------#
path = os.path.join(".","<intermediate_output_sentences>.txt")
file = open(path, 'r')
#----------------------------------------------------------------------#
def robot_output(i, s):
    output_robot_l = ["", ""]
    s = s[1:-3]
    tokens = s.split("] [")
    for t in tokens:
        if t.startswith('ARG0'):
            print(t[6:], "-> instrument")
            output_robot_l[0] = t[6:]
        if t.startswith('ARG1'):
            print(t[6:], "-> object")
            output_robot_l[1] = t[6:]
    return [i, output_robot_l]
        


def human_output(i, s):
    output_human_l = ["", "", ""]
    s = s[1:-3]
    tokens = s.split("] [")
    for t in tokens:
        if t.startswith('ARG0'):
            print(t[6:], "-> agent")
            output_human_l[0] = t[6:]
        if t.startswith('ARG1'):
            print(t[6:], "-> object")
            output_human_l[1] = t[6:]
        if t.startswith('ARG2') or t.startswith('ARG3'):
            print(t[6:], "-> instrument")
            output_human_l[2] = t[6:]
        if t.startswith('ARGM-MNR'):
            print(t[10:], "-> instrument")
            output_human_l[2] = t[10:]
    
    return [i, output_human_l]
#----------------------------------------------------------------------#
if __name__ == '__main__':
    #Each sentence contains a main action. Sentences with more main actions are duplicated
    #(each duplicate has the corresponding semantic annotations)
    sentences = genfromtxt(path, delimiter='@', encoding='utf8', dtype=str) 
    for i, s in enumerate(sentences):
        if s[0]=='0 ':
            result = robot_output(i, s[1].strip())
            print(result)
        else:
            result = human_output(i, s[1].strip())
            print(result)