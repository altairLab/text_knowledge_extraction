# AUTOMATE: From Text to ASP

_This site is still being updated. A more complete version (together with the fully automatic pipeline) will be added soon._

This repository contains packages and tools for extracting ASP rules from natural language text.
It also contains tools for autonomous tissue retraction with da Vinci Research Kit in a **simulated** environment, with ASP task logic either hand-written or automatically extracted from procedural text description.


<!--Overview:
* [Setup ROS](#Setup)
* [Description ASP](#Description)
* [Run](#Run)
* [References](#References)
-->

## Setup NLP
Semantic Role Labeling based on AllenNLP implementation of [Shi et al](https://arxiv.org/abs/1904.05255) , 2019. 
Required packages are allennlp (2.4.0), allennlp-models (2.4.0) and spacy (3.0.6). We used python 3.8.10.

## Setup ROS
This repo has been tested using ROS melodic (ubuntu 18.04) and python2.
##### ROS packages 
Build dvrk-ros package (v1.7.0), following instructions at https://github.com/jhu-dvrk/dvrk-ros (please note that building versions>1.7.0 will not work). In case you have problems building cisst because of missing header files, a temporary workaround is to go to the build/cisst directory and run `make -j -k` a few times. Once the headers have been generated, you can resuem the normal build process using `catkin build`.
##### Clingo 
Download and install Clingo (v5.3.0) following instructions at https://github.com/potassco/clingo (v5.3.0 is required for support with python2). Remember to add the PATH-TO-CLINGO/bin/python to your $PYTHONPATH to enable python to find the required modules.
##### SOFA 
Download and install SOFA (v20.06), following the instructions at https://www.sofa-framework.org/community/doc/getting-started/build/linux/. Simulation is implemented relying on SofaPython plugin, which needs to be manually activated in CMake (thus, python3 is not supported). Once built, SOFA can be launched with /PATH-TO-BUILD/bin/runSofa (you can either add runSofa to your ~/.bashrc or create a symlink). Required python modules for simulations can be found in simulation/requirements.txt.

## Description
- Code for text processing is in .\Text directory.
- dvrk_task_msgs contains custom msgs (action msgs expect fields defining the action id, the agent robot, the operated object and its property, as of standard granularity definitions for actions). **Note**: when running nodes on multiple machines, dvrk_task_msgs must be built on all of them.
- robot_control contains tools for dvrk control and motion control
- task_reasoning contains tools for ASP management with Clingo
- simulation contains tools for simulation in SOFA
- results for ISMR paper are in the task_reasoning/scripts/results

## Run
To start the framework, you have to
1. Start `roscore`
2. Start the simulated dVRK with `roslaunch robot_control dvrk_console.launch` (press "Power on" and then "Home" in the GUI)
3. Start the simulation: `runSofa scene_sofa_ros_double.py`
4. Start the autonomous framework with `roslaunch robot_control framework_tr.launch` (hand-written ASP encoding) or `roslaunch robot_control framework_tr_text.launch` (ASP from text). In both case, set the grid_size parameter in the launch file, to define the discretization of grasping points on the tissue.
5. Optional: you can visualize robot configuration in rviz with the scene robot_control/rviz_config/std_scene.rviz



##Contacts*: marco[dot]bombieri_01[at]univr[dot]it or daniele[dot]meli[at]univr[dot]it
Altair Robotics Lab - University of Verona

## Cite us!
If you are using this code, please cite this paper: _the paper will be added soon_

## License
Shield: [![CC BY-NC-SA 4.0][cc-by-nc-sa-shield]][cc-by-nc-sa]

This work is licensed under a
[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License][cc-by-nc-sa].

[![CC BY-NC-SA 4.0][cc-by-nc-sa-image]][cc-by-nc-sa]

[cc-by-nc-sa]: http://creativecommons.org/licenses/by-nc-sa/4.0/
[cc-by-nc-sa-image]: https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png
[cc-by-nc-sa-shield]: https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg
