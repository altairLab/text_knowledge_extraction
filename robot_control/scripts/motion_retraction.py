import numpy as np
import math
import rospy
import copy
from dvrk_task_msgs.msg import ObstArray, ActionArray, PoseStampedArray
from std_msgs.msg import Int32, Bool
from geometry_msgs.msg import PoseStamped, Pose
from tf import TransformListener
from tf_conversions import posemath as pm
import dvrk
import PyKDL














#MOTION CONTROL
class motion_manager(object):
    def __init__(self):
        self.dvrk_frame_id  = rospy.get_param("dvrk_frame_id")

        self.psm1 = dvrk.psm("PSM1")
        self.psm2 = dvrk.psm("PSM2")
        self.agents = []
        self.n_arms = [] #for dual-arm actions
        self.policies = []
        self.policy_types = []
        self.fixed_obst = True
        self.obstacles = ObstArray()
        self.target_pose = PoseStampedArray() 
        self.tissue_center = PoseStamped()
        self.state = [] #ASP action
        self.manip_id = [] #the name of the manipulator to be used to perform the prescribed action
        self.failure = False
        self.fluents = []
        self.location = []
        self.computed_fluents = False
        self.target_pose_listener = rospy.Subscriber('/target_pose', PoseStampedArray, self.target_pose_callback)
        self.state_listener = rospy.Subscriber('/actions/request', ActionArray, self.state_callback)
        self.failure_listener = rospy.Subscriber('/failure', Bool, self.failure_cb)
        self.computed_fluent_status_listener = rospy.Subscriber('/computed_fluents', Bool, self.computed_fluents_cb)
        self.tf_list = TransformListener()
        self.end_pub = rospy.Publisher('/action_feedback', Bool, queue_size=1)
        self.sensing_pub = rospy.Publisher('/ask_for_sensing', Int32, queue_size=1)
        self.obst_sub = rospy.Subscriber('/obstacles', ObstArray, self.obstacles_callback)
        self.tissue_center_sub = rospy.Subscriber("/tissue_center", PoseStamped, self.tissue_center_cb)

        self.motion_status_pub = rospy.Publisher("/motion_status", Bool, queue_size=1)

        self.motion_rate = 5.

        #RETRACTION SPECIFIC
        self.custom_policies = { "pull" : self.pull,
                                 "grasp" : self.grasp,
                                 "open" : self.open,
                                 "go_center" : self.go_center,
                                 "go_home" : self.go_home,
                                 "reach" : self.reach
                               } 

        self.init_pose1 = self.psm1.get_current_position()
        self.init_pose2 = self.psm2.get_current_position()


    def failure_cb(self, data):
        self.failure = data.data

    def computed_fluents_cb(self, data):
        self.computed_fluents = data.data
    
    def tissue_center_cb(self, data):
        self.tissue_center = data

    def target_pose_callback(self, data):
        self.target_pose = copy.deepcopy(data)

    def obstacles_callback(self, data):  
        self.obstacles = data   

    def state_callback(self, data):
        for i in range(len(data.action_list)):
            self.location.append(data.action_list[i].object)
            self.manip_id.append(data.action_list[i].robot)
            self.state.append(data.action_list[i].action)
    
    def go_home(self, idx):
        discretization = 20
        if self.agents[idx] == "psm1":
            psm = self.psm1
            init_pose = self.init_pose1
        elif self.agents[idx] == "psm2":
            psm = self.psm2
            init_pose = self.init_pose2
        current_pose = psm.get_current_position()
        step = [(init_pose.p[0] - current_pose.p[0]) / float(discretization), 
                    (init_pose.p[1] - current_pose.p[1]) / float(discretization),
                    (init_pose.p[2] - current_pose.p[2]) / float(discretization)]

    def pull(self, idx):
        self.sensing_pub.publish(Int32(2))
        rospy.sleep(2.)
        height = 0.01
        discretization = 20
        if self.agents[idx] == "psm1":
            psm = self.psm1
        elif self.agents[idx] == "psm2":
            psm = self.psm2

        rate = rospy.Rate(self.motion_rate)
        for _ in range(discretization):
            psm.dmove(PyKDL.Vector(0., 0., height/discretization), blocking=True)
            if self.failure:
                rospy.logwarn("INTERRUPTING MOTION (FORCE TOLERANCE)")
                break
            else:
                rate.sleep()
        
    def grasp(self, idx):
        if self.agents[idx] == "psm1":
            psm = self.psm1
        elif self.agents[idx] == "psm2":
            psm = self.psm2
        psm.close_jaw()

    def open(self, idx):
        height = 0.06
        discretization = 10
        width=math.pi/6.
        if self.agents[idx] == "psm1":
            psm = self.psm1
        elif self.agents[idx] == "psm2":
            psm = self.psm2
        psm.move_jaw(width)
        for _ in range(discretization):
            psm.dmove(PyKDL.Vector(0., 0., height/discretization), blocking=True)
    
    def go_center(self, idx):
        discretization = 40 #to avoid fast motion

        #ASK SENSING FOR TARGET AND ANOMALIES
        self.sensing_pub.publish(Int32(2))
        rospy.sleep(2.)
        while len(self.target_pose.poses) <= idx:
            pass
        #SELECT ARM
        if self.agents[idx] == "psm1":
            psm = self.psm1
        elif self.agents[idx] == "psm2":
            psm = self.psm2
        #TRANSFORM TO PSM BASE FRAME
        self.target_pose.poses[idx].header.stamp = rospy.Time(0.)
        self.tf_list.waitForTransform('world', self.agents[idx].upper()+'_base' + self.dvrk_frame_id, time=rospy.Time(0.), timeout = rospy.Duration(5.))
        target = self.tf_list.transformPose(self.agents[idx].upper()+'_base' + self.dvrk_frame_id, self.target_pose.poses[idx])
        #COMPUTE DIFF
        start = PyKDL.Vector(psm.get_current_position().p[0], psm.get_current_position().p[1], psm.get_current_position().p[2])
        end = PyKDL.Vector(target.pose.position.x, target.pose.position.y, psm.get_current_position().p[2])
        diff = end - start  
        #COMMAND DIFF  
        rate = rospy.Rate(self.motion_rate)
        step = 1./discretization
        for i in range(discretization + int(discretization/3.)): #move a little bit more than the ROI, to expose it better
            psm.move(start + diff*step*(i+1), blocking = True)
            rate.sleep()

    def reach(self, idx):
        discretization = 10 #to avoid fast motion

        #ASK SENSING FOR TARGET AND ANOMALIES
        self.sensing_pub.publish(Int32(2))
        rospy.sleep(2.)
        while len(self.target_pose.poses) <= idx:
            pass
        #SELECT ARM
        if self.agents[idx] == "psm1":
            psm = self.psm1
        elif self.agents[idx] == "psm2":
            psm = self.psm2
        #TRANSFORM TO PSM BASE FRAME
        self.target_pose.poses[idx].header.stamp = rospy.Time(0.)
        self.tf_list.waitForTransform('world', self.agents[idx].upper()+'_base' + self.dvrk_frame_id, time=rospy.Time(0.), timeout = rospy.Duration(5.))
        target = self.tf_list.transformPose(self.agents[idx].upper()+'_base' + self.dvrk_frame_id, self.target_pose.poses[idx])
        #COMPUTE DIFF
        start = PyKDL.Vector(psm.get_current_position().p[0], psm.get_current_position().p[1], psm.get_current_position().p[2])
        end = PyKDL.Vector(target.pose.position.x, target.pose.position.y, target.pose.position.z)
        diff = end - start  
        #COMMAND DIFF  
        rate = rospy.Rate(self.motion_rate)
        step = 1./discretization
        for i in range(discretization):
            psm.move(start + diff*step*(i+1), blocking = True)
            rate.sleep()

    def execute(self):
        if not self.failure:
            for i in range(len(self.policies)):
                self.custom_policies[self.policies[i]](i)

        self.motion_status_pub.publish(Bool(True))
        rospy.sleep(2.)

    def reset(self):
        self.failure = False
        self.state = []
        self.n_arms = []
        self.policies = []
        self.policy_types = []
        self.agents = []
        self.location = []
        self.manip_id = []