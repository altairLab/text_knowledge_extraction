#!/usr/bin/env python
import numpy as np
import math
import rospy
from std_msgs.msg import Header, Int32, Bool, Float32, Float32MultiArray as Flt_arr
from sensor_msgs.msg import PointCloud2
from sensor_msgs import point_cloud2 as pc2
from geometry_msgs.msg import PoseArray, PoseStamped, Pose, Point
from tf import TransformListener
from tf_conversions import posemath as pm
from quaternion import distance as orient_dist
from dvrk_task_msgs.msg import ContextModel, ActionArray, PoseStampedArray
import dvrk


#comm class
class Situation_awareness(object):
    def __init__(self):
        self.dvrk_frame_id  = rospy.get_param("dvrk_frame_id")
        self.grid_size = rospy.get_param("grid_size")

        self.psm1 = dvrk.psm('PSM1')
        self.psm2 = dvrk.psm('PSM2')
        self.psm1_standard_orient = None
        self.psm2_standard_orient = None
        self.old_fluents = []
        self.state = [] #FSM state
        self.manip_id = [] #the name of the manipulator to be used to perform the prescribed action
        self.block = [] #ID of the block as a matrix of 3D centroids
        self.block_id = [] #grid coordinates of blocks for ASP
        self.last_block_id = ["none", "none"] #to track last grasped block
        self.last_action = ["none", "none"] #to track last action
        self.excluded_fluents = [] #to track blocks to be excluded
        self.fixed = [] #ID of the block as a matrix of Bool
        self.failure = False
        self.location = []
        self.request = 0 # request ID --- 1 for fluents, 2 for failure detection
        self.tf_list = TransformListener()
        self.tissue_center = PoseStamped()
        self.roi_pose = PoseStamped()
        self.visible = 0. #percentage 0-100 of visibility of tumor
        #TO COMPUTE BLOCKS FROM PC2
        self.tissue_pts = []
        self.got_tissue = True
        self.fixed_pts = []
        self.forces = []
        self.force_failure = False
        self.max_force = 0.
        self.force_thr = 0.3
        self.max_height = 0.03
        self.visibility_thr = 70.
        self.computed_force = False
        self.discarded = []
        self.got_fixed = True
        self.dim_grid_x = self.grid_size #coordinates in simulation
        self.dim_grid_y = self.grid_size #coordinates in simulation
        self.init_time = rospy.Time.now()
        self.action_list_msg = ActionArray()
        self.max_allowed_time = 300.
        self.published_end = False

        self.fluent_pub = rospy.Publisher('/context/model', ContextModel, queue_size=1)
        self.target_pose_pub = rospy.Publisher('/target_pose', PoseStampedArray, queue_size=1)
        self.failure_pub = rospy.Publisher('/failure', Bool, queue_size=1)
        self.fluent_comp_status_pub = rospy.Publisher('/computed_fluents', Bool, queue_size=1)
        self.tissue_center_pub = rospy.Publisher("/tissue_center", PoseStamped, queue_size=1)
        self.test_pub = rospy.Publisher("/testing_blocks", PoseArray, queue_size=1)
        self.test_target_pub = rospy.Publisher("/testing_target", PoseStamped, queue_size=1)

        #FOR TEST ISMR
        self.end_pub = rospy.Publisher("/ended_test", Bool, queue_size=1)
        self.action_pub = rospy.Publisher("/action_test", ActionArray, queue_size=1)
        #FOR TEST ISMR

        self.tissue_listener = rospy.Subscriber('/sofa_tissue_state', PointCloud2, self.tissue_cb)
        self.fixed_listener = rospy.Subscriber('/sofa_fixed', PointCloud2, self.fixed_cb)
        self.state_listener = rospy.Subscriber('/actions/request', ActionArray, self.state_callback)
        self.sensing_listener = rospy.Subscriber('/ask_for_sensing', Int32, self.on_sensing_request)
        self.visible_listener = rospy.Subscriber("/target_visible_perc", Float32, self.visible_callback)
        self.roi_pos_listener = rospy.Subscriber("/target_position", Flt_arr, self.roi_pos_callback)
        self.motion_status_listener = rospy.Subscriber("/motion_status", Bool, self.motion_status_cb)
        self.force_listener = rospy.Subscriber("/sofa_forces", Flt_arr, self.force_callback)



    def on_sensing_request(self, data):
        self.request = data.data
    
    def visible_callback(self, data):
        self.visible = data.data

    def roi_pos_callback(self, data):
        self.roi_pose = PoseStamped(header=Header(stamp = rospy.Time(0.), frame_id="world"), 
                                    pose = Pose(position=Point(x=data.data[0], y=data.data[1], z=data.data[2])))

    def force_callback(self, data):
        forces = []
        for i in range(int(len(data.data)/3)):
            forces.append(np.linalg.norm([data.data[3*i], data.data[3*i+1], data.data[3*i+2]]))
        
        #EXCLUDE OUTLIERS
        if len(self.forces) < 10: 
            self.forces.append(max(forces))
        else:
            self.max_force = np.median(self.forces)
            self.forces = []  

        # if self.max_force > self.force_thr:
        #     rospy.loginfo("MAX FORCE IS " + str(self.max_force))

    def motion_status_cb(self, data):
        state = self.state
        manip_id = self.manip_id
        block_id = self.block_id
        for i in range(len(state)):
            if state[i] == "pull":       
                if manip_id[i] == "psm1":
                    psm = self.psm1
                else:
                    psm = self.psm2
                psm_base = PoseStamped(header = Header(stamp = rospy.Time(0.), frame_id = psm.name().upper() + "_base"),
                                            pose = pm.toMsg(psm.get_current_position()))
                self.tf_list.waitForTransform("world", psm.name().upper() + "_base"+self.dvrk_frame_id, time=psm_base.header.stamp, timeout=rospy.Duration(5.))
                psm_world = self.tf_list.transformPose("world", psm_base)

                block_id_str = block_id[i].replace("(", "").replace(")", "").replace(" ", "")
                comma = block_id_str.find(",")
                block_id = (int(block_id_str[0:comma])-1, int(block_id_str[(comma+1):])-1)
                self.block[block_id[0]][block_id[1]][2] = psm_world.pose.position.z
            # elif self.state[i] == "go_center":
            #     if self.manip_id[i] == "psm1":
            #         psm = self.psm1
            #     else:
            #         psm = self.psm2
            #     psm_base = PoseStamped(header = Header(stamp = rospy.Time(0.), frame_id = psm.name().upper() + "_base"),
            #                                 pose = pm.toMsg(psm.get_current_position()))
            #     self.tf_list.waitForTransform("world", psm.name().upper() + "_base"+self.dvrk_frame_id, time=psm_base.header.stamp, timeout=rospy.Duration(5.))
            #     psm_world = self.tf_list.transformPose("world", psm_base)

            #     block_id_str = self.block_id[i].replace("(", "").replace(")", "").replace(" ", "")
            #     comma = block_id_str.find(",")
            #     block_id = (int(block_id_str[0:comma])-1, int(block_id_str[(comma+1):])-1)
            #     self.block[block_id[0]][block_id[1]][0] = psm_world.pose.position.x
            #     self.block[block_id[0]][block_id[1]][1] = psm_world.pose.position.y
            elif state[i] == "open":
                for i in range(np.shape(self.block)[0]):
                    for j in range(np.shape(self.block)[1]):
                        self.block[i][j][2] = 0
                

    def state_callback(self, data):
        if len(data.action_list) > 0:
            self.state = []
            self.location = []
            self.manip_id = []
            self.block_id = []
            for i in range(len(data.action_list)):
                self.state.append(data.action_list[i].action)
                self.location.append(data.action_list[i].object)
                self.manip_id.append(data.action_list[i].robot)
                self.block_id.append(data.action_list[i].color)
            for i in range(len(self.block_id)):
                if self.block_id[i] != "none":
                    self.last_block_id[i] = self.block_id[i]
            for i in range(len(self.state)):
                self.last_action[i] = self.state[i]
            self.action_list_msg.action_list += data.action_list


    def tissue_cb(self, data):
        if self.tissue_pts == []:
            gen = pc2.read_points(data, skip_nans=True, field_names=["x", "y", "z"])
            for point in gen:
                self.tissue_pts.append(point)
        #COMPUTE CENTER OF TISSUE
        if self.tissue_center == PoseStamped():
            for point in self.tissue_pts:
                self.tissue_center.pose.position.x += point[0]
                self.tissue_center.pose.position.y += point[1]
                self.tissue_center.pose.position.z += point[2]
            self.tissue_center.pose.position.x /= len(self.tissue_pts)
            self.tissue_center.pose.position.y /= len(self.tissue_pts)
            self.tissue_center.pose.position.z /= len(self.tissue_pts)
            self.tissue_center.header = data.header
        # else:
            # self.tissue_center_pub.publish(self.tissue_center)

        self.got_tissue = True

    def fixed_cb(self, data):
        gen = pc2.read_points(data, skip_nans=True, field_names=["x", "y", "z"])
        for point in gen:
            self.fixed_pts.append(point)
        if self.fixed_pts != []:
            self.got_fixed = True
        
    def gen_blocks(self):
        #COMPUTE VERTICES OF BLOCKS
        tissue_pts = self.tissue_pts
        x_tissue = [p[0] for p in tissue_pts]
        y_tissue = [p[1] for p in tissue_pts]
        z_tissue = [p[2] for p in tissue_pts]
        step_x = (max(x_tissue) - min(x_tissue)) / self.dim_grid_x
        x_vert_blocks = [min(x_tissue) + i*step_x for i in range(self.dim_grid_x+1)]
        step_y = (max(y_tissue) - min(y_tissue)) / self.dim_grid_y
        y_vert_blocks = [min(y_tissue) + i*step_y for i in range(self.dim_grid_y+1)]
        z_mean = np.mean(z_tissue) #axis of thickness, not used for block computation
        #ASSIGN POINTS TO CLUSTERS
        clust_pts = np.empty(self.dim_grid_x*self.dim_grid_y, np.object)
        for i in enumerate(clust_pts):
            clust_pts[i] = [[0., 0., z_mean, 0.]]
        clust_pts = np.reshape(clust_pts, (self.dim_grid_x, self.dim_grid_y))
        for p in tissue_pts:
            for i in range(self.dim_grid_x):
                for j in range(self.dim_grid_y):
                    if p[0] >= x_vert_blocks[i] and p[0] < x_vert_blocks[i+1] and p[1] >= y_vert_blocks[j] and p[1] < y_vert_blocks[j+1]:
                        clust_pts[i][j][0] += p[0]
                        clust_pts[i][j][1] += p[1]
                        clust_pts[i][j][3] += 1.
        #COMPUTE CENTROIDS OF BLOCKS
        for i in range(self.dim_grid_x):
            for j in range(self.dim_grid_y):
                clust_pts[i][j] = [clust_pts[i][j][0] / clust_pts[i][j][3], clust_pts[i][j][1] / clust_pts[i][j][3], clust_pts[i][j][2]]
        self.block = clust_pts
        #ASSIGN FIXED POINTS TO CLUSTERS
        fixed_pts = self.fixed_pts
        self.fixed = [False for _ in range(self.dim_grid_y*self.dim_grid_x)] #[y, z, # points]
        self.fixed = np.reshape(self.fixed, (self.dim_grid_x, self.dim_grid_y))
        for p in fixed_pts:
            for i in range(self.dim_grid_x):
                for j in range(self.dim_grid_y):
                    if p[0] >= x_vert_blocks[i] and p[0] < x_vert_blocks[i+1] and p[1] >= y_vert_blocks[j] and p[1] < y_vert_blocks[j+1]:
                        self.fixed[i][j] = True

    def reach_target(self, idx):
        block_id = self.block_id
        #SELECT TARGET BLOCK
        block_id_str = block_id[idx].replace("(", "").replace(")", "").replace(" ", "")
        comma = block_id_str.find(",")
        block_id = (int(block_id_str[0:comma])-1, int(block_id_str[(comma+1):])-1)
        return PoseStamped(header = Header(frame_id = "world", stamp = rospy.Time.now()), 
                            pose = Pose(position = Point(x = self.block[block_id[0]][block_id[1]][0],
                                                            y = self.block[block_id[0]][block_id[1]][1])))


    def go_target(self, idx):
        manip_id = self.manip_id
        if manip_id[idx] == "psm1":
            psm = self.psm1
        elif manip_id[idx] == "psm2":
            psm = self.psm2
        #TRANSFORM TO PSM BASE FRAME
        pose_base = pm.toMsg(psm.get_current_position())
        pose_base = PoseStamped(header = Header(frame_id=manip_id[idx].upper()+'_base' + self.dvrk_frame_id, stamp=rospy.Time(0.)), pose=pose_base)
        self.tf_list.waitForTransform("world", manip_id[idx].upper()+'_base' + self.dvrk_frame_id, time=pose_base.header.stamp, timeout = rospy.Duration(10.))
        pose_world = self.tf_list.transformPose("world", pose_base)

        #TARGET IS CENTER OF TISSUE
        return PoseStamped(header = Header(frame_id = "world", stamp = rospy.Time.now()), 
                            pose = Pose(position = Point(x = self.tissue_center.pose.position.x,
                                                            y = self.tissue_center.pose.position.y, 
                                                            z = pose_world.pose.position.z)))
    def compute_target(self):  
        target = PoseStampedArray()
        state = self.state
        for i in range(len(state)):
            if state[i] == "reach":
                target.poses.append(self.reach_target(i))
                self.test_target_pub.publish(target.poses[-1])
            elif state[i] == "go_center":
                target.poses.append(self.go_target(i))

        self.target_pose_pub.publish(target)

    def compute_fluents(self):
        fluents = []
        # distances = []

        #visibility of tumor
        if self.visible > self.visibility_thr or rospy.Time.now() - self.init_time > rospy.Duration(self.max_allowed_time):
            self.request = 0
            fluents.append("visible(roi)")
            self.action_pub.publish(self.action_list_msg)
            if not self.published_end:
                self.end_pub.publish(Bool(True))
                self.published_end = True
        elif "go_center" in self.last_action:
            i = self.last_action.index("go_center")
            self.excluded_fluents.append("excluded(" + self.last_block_id[i] + ")")


        #WAIT FOR PC2 TO BE PUBLISHED
        while not self.got_tissue or not self.got_fixed:
            pass
        #GENERATE ATOMS FOR FIXED BLOCKS
        while self.block == [] or self.fixed_pts == []: #generate blocks only at the beginning of the task
            try:
                self.gen_blocks()
            except:
                # rospy.logwarn("RECEIVING EMPTY POINT CLOUDS?")
                pass

        #COMPUTE HEIGHT OF PSMs
        psm1_base = PoseStamped(header = Header(stamp = rospy.Time(0.), frame_id = "PSM1_base"),
                                    pose = pm.toMsg(self.psm1.get_current_position()))
        self.tf_list.waitForTransform("world", "PSM1_base"+self.dvrk_frame_id, time=psm1_base.header.stamp, timeout=rospy.Duration(5.))
        psm1_world = self.tf_list.transformPose("world", psm1_base)
        psm2_base = PoseStamped(header = Header(stamp = rospy.Time(0.), frame_id = "PSM2_base"),
                                    pose = pm.toMsg(self.psm2.get_current_position()))
        self.tf_list.waitForTransform("world", "PSM2_base"+self.dvrk_frame_id, time=psm2_base.header.stamp, timeout=rospy.Duration(5.))
        psm2_world = self.tf_list.transformPose("world", psm2_base)

        if psm1_world.pose.position.z > self.max_height:
            fluents.append("max_height(psm1)")
        if psm2_world.pose.position.z > self.max_height:
            fluents.append("max_height(psm2)")


        where_fixed = np.where(self.fixed == True)
        for i in range(np.shape(where_fixed[0])[0]):
            fluents.append("fixed((" + str(where_fixed[0][i] + 1) + "," + str(where_fixed[1][i] + 1) + "))")
        fluents += self.excluded_fluents

        #gripper status
        if self.psm2.get_current_jaw_position() < math.pi / 8.:
            fluents.append("closed_gripper(psm2)")
        if self.psm1.get_current_jaw_position() < math.pi / 8.:
            fluents.append("closed_gripper(psm1)")

        for i in range(np.shape(self.block)[0]):
            for j in range(np.shape(self.block)[1]):
                #PSM1
                dist = np.linalg.norm([psm1_world.pose.position.x - self.block[i][j][0],
                                        psm1_world.pose.position.y - self.block[i][j][1],
                                        psm1_world.pose.position.z - self.block[i][j][2]])
                # rospy.loginfo("DISTANCE 1 IS" + str(dist))
                if dist < 0.005:
                    if self.psm1.get_current_jaw_position() < math.pi / 8.:
                        fluents.append("in_hand(psm1,tissue,(" + str(i+1) + "," + str(j+1) + "))")
                    fluents.append("at(psm1,tissue,(" + str(i+1) + "," + str(j+1) + "))")
                #PSM2
                dist = np.linalg.norm([psm2_world.pose.position.x - self.block[i][j][0],
                                        psm2_world.pose.position.y - self.block[i][j][1],
                                        psm2_world.pose.position.z - self.block[i][j][2]])
                # rospy.loginfo("DISTANCE 2 IS" + str(dist))
                if dist < 0.005:
                    if self.psm2.get_current_jaw_position() < math.pi / 8.:
                        fluents.append("in_hand(psm2,tissue,(" + str(i+1) + "," + str(j+1) + "))")
                    fluents.append("at(psm2,tissue,(" + str(i+1) + "," + str(j+1) + "))")

        for i in range(np.shape(self.block)[0]):
            for j in range(np.shape(self.block)[1]):
                dist1 = np.linalg.norm([psm1_world.pose.position.x - self.block[i][j][0],
                                        psm1_world.pose.position.y - self.block[i][j][1],
                                        psm1_world.pose.position.z - self.block[i][j][2]]) 
                dist2 = np.linalg.norm([psm2_world.pose.position.x - self.block[i][j][0],
                                        psm2_world.pose.position.y - self.block[i][j][1],
                                        psm2_world.pose.position.z - self.block[i][j][2]]) 
                if dist1 > dist2:
                    fluents.append("distance(psm1,(" + str(i+1) + "," + str(j+1) + "),2)")
                    fluents.append("distance(psm2,(" + str(i+1) + "," + str(j+1) + "),1)")
                else:
                    fluents.append("distance(psm1,(" + str(i+1) + "," + str(j+1) + "),1)")
                    fluents.append("distance(psm2,(" + str(i+1) + "," + str(j+1) + "),2)")

        if self.force_failure:
            self.force_failure = False
            fluents.append("max_force")
        
        #SEND FLUENTS
        if self.request != 0:
            msg = ContextModel()
            for i in range(len(fluents)):
                msg.atoms.append(fluents[i])
            self.fluent_pub.publish(msg)
            self.request = 0
            self.fluent_comp_status_pub.publish(Bool(True))
            rospy.sleep(1.)

    def reset(self):
        self.failure = False
        self.request = 0
    
    def failure_pull(self):
        if self.max_force > self.force_thr:
            self.failure = True
            self.force_failure = True
            self.max_force = 0.
            self.forces = []

    def check_failure(self):
        state = self.state
        for i in range(len(state)):
            if state[i] == "pull":
                self.failure_pull()

            







def main():

    rospy.init_node('sensing_node')
    sa = Situation_awareness()
    rospy.sleep(1.)

    while not rospy.is_shutdown():
        # TESTING 
        # if sa.block != []:
        #     arr = PoseArray()
        #     arr.header.stamp = rospy.Time.now()
        #     arr.header.frame_id = "world"
        #     for i in range(np.shape(sa.block)[0]):
        #         for j in range(np.shape(sa.block)[1]):
        #             arr.poses.append(Pose(position = Point(x = sa.block[i][j][0], y = sa.block[i][j][1], z = sa.block[i][j][2])))
        #     sa.test_pub.publish(arr)

        if sa.request == 1:
            sa.compute_fluents()
        elif sa.request == 2:
            sa.compute_target()
            sa.check_failure()
            if sa.failure:
                sa.reset()
                sa.failure_pub.publish(Bool(True))

    rospy.spin()







if __name__ == '__main__':
    main()



