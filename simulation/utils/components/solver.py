class Solver:
    STATIC = 'Static'
    DYNAMIC = 'Dynamic'

    CG = 'CG'
    PARDISO = 'Pardiso'
    SOFASPARSE = 'SofaSparse'
    
    def __init__(self, parent_node, analysis=DYNAMIC, solver='CG', solver_name='Solver', **kwargs):
        assert analysis in [self.STATIC, self.DYNAMIC]
        self.analysis = analysis
        assert solver in [self.CG, self.PARDISO, self.SOFASPARSE]
        solver_type = solver

        # Analysis
        if self.analysis == self.DYNAMIC:
            kd = kwargs.get('kd', None)
            ks = kwargs.get('ks', None)
            self.odesolver = parent_node.createObject('EulerImplicitSolver')
            if kd is not None:
                self.odesolver.rayleighMass = kd
            if ks is not None:
                self.odesolver.rayleighStiffness = ks

        elif self.analysis == self.STATIC:
            self.newton_iter = kwargs.get('newton_iterations',20)
            parent_node.createObject('StaticSolver', newton_iterations=self.newton_iter, correction_tolerance_threshold=1e-7, residual_tolerance_threshold=1e-7)
            
        # Linear solver   
        log = kwargs.get('printLog', False)
        if solver_type == self.CG:
            num_iter = kwargs.get('num_iter', 100)
            self.solver = parent_node.createObject('CGLinearSolver',name=solver_name,iterations=num_iter,threshold="1e-15",printLog=log)

        elif solver_type == self.PARDISO:
            try:
                parent_node.createObject('RequiredPlugin',name='SofaPardisoSolver',pluginName='SofaPardisoSolver')
                self.solver = parent_node.createObject('SparsePARDISOSolver',name=solver_name,printLog=log)
            except Exception as e:
                print "Exception. To use Pardiso solver you need SofaPardisoSolver plugin, which is private."
                raise e

        elif solver_type == self.SOFASPARSE: 
            parent_node.createObject('RequiredPlugin',name='SofaSparseSolver',pluginName='SofaSparseSolver')
            self.solver = parent_node.createObject('SparseLDLSolver',name=solver_name,printLog=log)
