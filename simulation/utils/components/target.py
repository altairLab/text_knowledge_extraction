import numpy as np
import time
import random
import sofa_utils

class Target():
	"""
	Mechanical object defining the target position.
	"""
	# SOFA components
	node    = None 
	state   = None

	#########################################################
	####### CREATION
	#########################################################
	def __init__(self, parent_node, **kwargs):
		""" 
		Keyword arguments:
			parent_node (Sofa Node):
				parent node to which the object is attached.
			view (bool): 
				If true, the target is visualized (default True).
			surface_mesh (str): 
				Path to the surface mesh of the simulated object (default None).
			name (str):  
				Name of the node where the object is created (default 'SofaObject').
		"""
		self.parent_node = parent_node
		self.view = kwargs.get('view', True)
		self.surface_mesh = kwargs.get('surface_mesh', None)
		self.node_name = kwargs.get('name', 'Target')

		# creates the graph
		self.__create_sofa_graph()

	def __create_sofa_graph(self):
		target_node = self.parent_node.createChild(self.node_name)
		
		# Mechanical object
		self.state = target_node.createObject('MechanicalObject', position=[0,0,0], name=self.node_name+'_state', showObject=False)
		self.__visualize_mechanical()
		
		self.node = target_node

		# Surface mesh
		if self.view and not(self.surface_mesh):
			print 'Please provide a surface mesh for the target if you want to visualize it'
			self.view = False

		if self.view:
			self.visualize( self.surface_mesh )
		

	#########################################################
	####### CUSTOM
	#########################################################
	def set_position( self, new_position ):
		if not isinstance(new_position, list):
			new_position = list(new_position)
		self.state.position = new_position
		self.__set_visual_position( new_position )
		return new_position
	
	def set_rnd_position( self, bounds=[0,0,0,1,1,1], seed=None ):
		# Bounds are expressed as [xmin, ymin, zmin, xmax, ymax, zmax]
		if seed is None:
			seed = time.time()
		random.seed(seed)
		xt = random.uniform(bounds[0], bounds[0+3])
		yt = random.uniform(bounds[1], bounds[1+3])
		zt = random.uniform(bounds[2], bounds[2+3])
		new_position = [xt, yt, zt]
		self.state.position = new_position
		self.__set_visual_position( new_position )
		return new_position

	def visualize( self, surface_mesh, color=[1, 0, 1, 0.9]  ):
		visual_ee_position = sofa_utils.convert2rigid( self.state.position )
		node_name = 'VisualNode'
		visual_ee_node = self.node.createChild(node_name) 
		self.visual_state = visual_ee_node.createObject('MechanicalObject',name='VisualT', position=visual_ee_position.tolist(), template='Rigid3d', showObject=0)

		visual_ee_mesh_node = visual_ee_node.createChild('Visual')
		visual_ee_mesh_node.createObject('MeshSTLLoader', name='Tloader', filename=surface_mesh)
		visual_ee_mesh_node.createObject('TriangleSetTopologyContainer', name='Ttopology', src='@Tloader')
		visual_ee_mesh_node.createObject('OglModel', color=color) 
		visual_ee_mesh_node.createObject('RigidMapping')

	#########################################################
	####### CUSTOM - private
	#########################################################
	def __set_visual_position( self, position ):
		if self.visual_state is not None:
			visual_position = sofa_utils.convert2rigid( position )
			self.visual_state.position = visual_position.tolist()

	def __visualize_mechanical( self, scale=1, color=[1,1,1,1] ):
		self.state.showObject=1
		self.state.showObjectScale=scale
		self.state.showColor=color