import numpy as np

class FixedBoundaries:
    
    def __init__(self, parent_node, indices=0, roi_node=None, node_name='FixedConstraint', view=1, **kwargs):
        self.indices = indices

        if roi_node:
            self.indices = roi_node.getLinkPath()+'.indices'
        
        fixed_node = parent_node.createObject(
                    'FixedConstraint', 
                    indices=self.indices,
                    listening='1',
                    name=node_name,
                    showObject=view
                    )
        self.node = fixed_node



class SpringBoundaries:

    def __init__( self, name, organ, startIndices, endPoints, stiffness=None, length=None ):
        """
        Parameters
        ----------
        name (str):
            Name to use for new node
        organ (SofaObject):
            The mesh to which the ligament will be connected
        startIndices (list of int):
            List point indices of organ mesh
        endPoints (list of Tuple(3)):
            List of points to connect the springs to
        stiffness (None or list of float):
            List of stiffnesses, one value per spring.
            If None is given, the default of 10 will be used for every spring.
        length (None or list of float):
            List of lengths, one value per spring.
            If None is given, the default of 0 will be used for every spring.
        """
        if stiffness is None:
            stiffness = [10]*len(startIndices)
        if length is None:
            length = [0]*len(startIndices)

        if not hasattr(organ, 'springNodes'):
            organ.springNodes = []

        removeIndices = []
        for index in startIndices:
            if index in organ.springNodes:
                removeIndices.append( index )
        startIndices = [j for i, j in enumerate(startIndices) if i not in removeIndices]
        endPoints = [j for i, j in enumerate(endPoints) if i not in removeIndices]
        stiffness = [j for i, j in enumerate(stiffness) if i not in removeIndices]
        length = [j for i, j in enumerate(length) if i not in removeIndices]

        #DEBUG:
        stiffness = [s for s in stiffness]

        organ.springNodes += startIndices

        assert len(startIndices) == len(endPoints), "Number of startIndices must be the same as number of endPoints."
        assert len(startIndices) == len(stiffness), "Number of startIndices must be the same as number of stiffness values."
        assert len(startIndices) == len(length), "Number of startIndices must be the same as number of length values."

        self.parent_node = organ.node
        self.node = self.parent_node.createChild( name )
        pos = []
        for p in endPoints:
            pos += [p[0], p[1], p[2]]
        self.node.createObject("PointSetTopologyContainer", name="end_points_topology",
                position=pos)
        #self.node.createObject("Mesh", name="end_points_mesh", src="@end_points_topology" )
        self.node.createObject('MechanicalObject', name='state', showObject=True,
                showObjectScale='2')
        self.node.createObject('FixedConstraint', name='fixing',
                indices=range(len(endPoints)))

        self.startIndices = startIndices
        self.endPoints = endPoints

        springVals = []
        for i in range( len(self.endPoints) ):
            springVals += [self.startIndices[i], i, stiffness[i], 0, length[i]]
        self.springs = self.node.createObject( "StiffSpringForceField", name="Springs",
                spring=springVals, object1="@../behavior_state", object2="@." )
        #self.node.createObject( "StiffSpringForceField", spring=springVals,
        #        object1="@../SurfaceMechanical/surface_state", object2="@." )

