import os, math, sys
import numpy as np
import Sofa

import components as comp
from sofa_utils import get_bbox, check_valid_displacement, get_distance_np

class Material:
	LINEAR = 'Linear'
	COROTATED = 'Corotated'
	STVENANTKIRCHHOFF = 'StVenantKirchhoff'
	NEOHOOKEAN = 'NeoHookean'

	def __init__(self, constitutive_model=LINEAR, poisson_ratio=0.3, young_modulus=4000, mass_density=2.5):
		assert constitutive_model in [self.LINEAR, self.COROTATED, self.STVENANTKIRCHHOFF, self.NEOHOOKEAN]
		self.constitutive_model = constitutive_model
		self.poisson_ratio = poisson_ratio
		self.young_modulus = young_modulus
		self.mass_density = mass_density


class Tissue(Sofa.PythonScriptController):
	"""
	Mechanical object describing a deformable tissue.
	"""
	# SOFA components
	node    = None 
	state   = None
	fem     = None

	# Tissue state
	bounding_box = None
	is_stable = True

	#########################################################
	####### CREATION
	#########################################################
	def __init__(self, parent_node, **kwargs):
		""" 
		Keyword arguments:
			parent_node (Sofa Node):
				parent node to which the object is attached.
			tetra_mesh (str):
				Path to the tetrahedral mesh of the simulated object (default None).
			material (Material): 
				Material used for the elastic force field.
			surface_mesh (str): 
				Path to the surface mesh of the simulated object (default None).
			name (str):  
				Name of the node where the object is created (default 'SofaObject').
			view (bool):
				if True, surface is visualized. Requires a surface mesh.
		"""
		self.parent_node = parent_node
		self.volume_mesh = kwargs.get('tetra_mesh')
		self.surface_mesh = kwargs.get('surface_mesh', None)
		self.material = kwargs.get('material', Material())
		self.node_name = kwargs.get('name', 'Tissue')
		self.view = kwargs.get('view', False)

		# Check on the material
		assert isinstance(self.material, Material)

		# creates the graph
		self.__create_sofa_graph()

	def __create_sofa_graph(self):
		tissue_node = self.parent_node.createChild(self.node_name)

		# Surface mesh
		if self.surface_mesh:
			surface_mesh_name = 'surface_mesh'
			surface_mesh_loader = comp.load_mesh(tissue_node,filename=self.surface_mesh,node_name=surface_mesh_name)
			self.surface_mesh_loader = surface_mesh_loader

		# Tetrahedral mesh
		volume_mesh_name = 'volume_mesh'
		comp.load_mesh(tissue_node,filename=self.volume_mesh,node_name=volume_mesh_name)
		self.tetra_mesh_topology = comp.create_tetrahedral_topology(tissue_node,volume_mesh=volume_mesh_name)
		
		# Mechanical object
		self.state = tissue_node.createObject('MechanicalObject', src='@Topology', name=self.node_name+'_state', showObject=False)
		
		# Force field
		self.fem = comp.create_tetrahedral_FEM(tissue_node,self.material)

		xmin, xmax, ymin, ymax, zmin, zmax = get_bbox( self.tetra_mesh_topology.position )
		self.bounding_box = [xmin, ymin, zmin, xmax, ymax, zmax]

		self.node = tissue_node

		# Visualization
		if self.view:
			self.visualize_surface()

	#########################################################
	####### SOFA-RELATED
	#########################################################
	def bwdInitGraph(self, rootNode):
		self.previous_pos = np.asarray(self.state.rest_position)

	def onEndAnimationStep(self,dt):
		# Check for simulation instability at the end of each time step
		current_pos = np.asarray(self.state.position)
		displ = current_pos - self.previous_pos
		self.previous_pos = current_pos

		if not check_valid_displacement(displ,low_thresh=-0.01):
			self.is_stable = False

	def reset(self):
		self.state.position = self.state.rest_position

	#########################################################
	####### CUSTOM
	#########################################################
	def visualize_surface( self, color=[1, 1, 0, 1] ):
		if self.surface_mesh:
			visual_node = self.node.createChild('VisualSurface')
			visual_node.createObject('OglModel', name="VisualSurfaceOGL", src="@../surface_mesh", color=color, listening=1)
			visual_node.createObject('BarycentricMapping', input="@../"+self.state.name, output="@VisualSurfaceOGL")

	def compute_von_mises( self ):
		self.fem.listening = 1
		self.fem.computeVonMisesStress = 2 # 2=using full Green tensor
		self.fem.showVonMisesStressPerNode = 1

	def compute_spherical_roi( self, center_position, radius ):
		dist, idx = get_distance_np( center_position, self.state.position )
		indices = np.where(dist <= radius)[0]
		return indices.tolist()
	
	#########################################################
	####### CUSTOM - private
	#########################################################
	def __visualize_mechanical( self, scale=2, color=[1,1,1,1] ):
		self.state.showObject=1
		self.state.showObjectScale=scale
		self.state.showColor=color

