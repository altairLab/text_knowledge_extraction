from numpy import linalg as LA
import numpy as np
from scipy import spatial

def get_bbox(position):
	""" 
	Gets the bounding box of the object defined by the given vertices.

	Arguments
	-----------
	position : list
		List with the coordinates of N points (position field of Sofa MechanicalObject).

	Returns
	----------
	xmin, xmax, ymin, ymax, zmin, zmax : floats
		min and max coordinates of the object bounding box.
	"""
	points_array = np.asarray(position)
	m = np.min(points_array, axis=0)
	xmin, ymin, zmin = m[0], m[1], m[2]

	m = np.max(points_array, axis=0)
	xmax, ymax, zmax = m[0], m[1], m[2]

	return xmin, xmax, ymin, ymax, zmin, zmax

def check_valid_displacement(displacement, low_thresh=0.0, high_thresh=np.nan):
	""" 
	Computes if the displacement is valid.

	Arguments
	-----------
	displacement : array_like
		Nx3 array with x,y,z displacements of N points.
	low_thresh : float
		value below which the displacement is discarded (defalut 0.0).
	high_thresh : float
		value above which the displacement is discarded (defalut NaN).
	
	Returns
	-----------
	False, if:
		* there is at least one displacement with NaN value
		* there is at least one displacement whose norm is >= high_thresh
		* there is at least one displacement whose norm is <= low_thresh 
	otherwise, returns True.
	"""
	displ_norm = LA.norm(displacement, axis=1)
	max_displ_norm = np.amax(displ_norm)

	if np.isnan(high_thresh): 
		if np.isnan(max_displ_norm):
			print 'Simulation is unstable'
			return False
	elif max_displ_norm >= high_thresh:
		print 'Max displacement is too big'
		return False

	if max_displ_norm <= low_thresh:
		print 'Max displacement is too small'
		return False

	return True

def get_distance_np( array1, array2 ):
	"""
	For each point in array2, returns the Euclidean distance with its closest point in array1 and
	the index of such closest point. 
	
	Arguments
	----------
	array1 (ndarray):
		N1 x M array with the points.
	array2 (ndarray):
		N2 x M array with the points.
	
	Returns
	----------
	dist:
		N2 x 1 Euclidean distance of each point of array2 with the closest point in array1.
		dist contains the same result as: 	np.nanmin( distance.cdist( array1, array2 ), axis=1 )
	indexes:
		N2 x 1 Indices of closest point in array1

	"""
	mytree = spatial.cKDTree(array1)
	dist, indexes = mytree.query(array2)
	return dist, indexes

def lame(material):
	"""
	Given a material, converts the specified young modulus and poisson ratio into lame parameters,
	depending on the specified material (St Venant Kirchhoff or Neo-Hookean).

	Arguments
	----------
	material
	
	Returns
	----------
	list:
		lame parameters
	"""
	young_modulus = material.young_modulus
	poisson_ratio = material.poisson_ratio
	if material.constitutive_model == 'StVenantKirchhoff':
		mu = young_modulus / (2. * (1. + poisson_ratio))
		l = young_modulus * poisson_ratio / ((1. + poisson_ratio) * (1. - 2.*poisson_ratio))
		parameters = [mu,l]
	elif material.constitutive_model == 'NeoHookean':
		mu = young_modulus / (2. * (1. + poisson_ratio))
		K = young_modulus /(3 * (1. - 2.*poisson_ratio))
		parameters = [mu,K]
	return parameters

def from_lame(mu, l):
	poisson_ratio = 1. / (2 * ((mu / l) + 1))
	young_modulus = 2*mu*(1 + poisson_ratio)
	return poisson_ratio, young_modulus

def convert2rigid( positions ):
	"""
	Converts an Nx3 list of coordinates, that describe 3D coordinates of a SOFA object templated as Vec3d template into
	the corresponding Nx7 (poosition+quaternion) format, needed for SOFA Rigid template.  
	
	Arguments
	----------
	positions (list):
		N x 3 list of coordinates.
	
	Returns
	----------
	list:
		N x 7 list of coordinates concatenated with a unit quaternion, for compatibility with Rigid template.
	"""
	positions = np.asarray( positions )
	positions = positions.reshape((-1,3))
	num_positions = positions.shape[0]
	quat = np.tile([0,0,0,1], (num_positions, 1))
	rigid_positions = np.hstack( (positions, quat) )
	return rigid_positions

