import random, math
import numpy as np
from vtk import *
import time
import utils

def geodesicDistance( surface, centerNodeID ):
 
	# pre-compute cell neightbors:
	neighbors = {}
	for i in range( surface.GetNumberOfPoints() ):
		neighbors[i] = getConnectedVertices( surface, i )
	
	distance = vtkDoubleArray() 
	distance.SetNumberOfTuples( surface.GetNumberOfPoints() )
	distance.SetNumberOfComponents( 1 )
	distance.Fill( 1e10 )   # initialize with large numbers
	distance.SetName( "geodesic_distance" )

	front = [centerNodeID]
	distance.SetTuple1( centerNodeID, 0 )

	while len(front) > 0:

		curID = front.pop(0)
		curPt = surface.GetPoint( curID )
		curDist = distance.GetTuple1( curID )
		curNeighbors = neighbors[curID]

		# Go through all neighboring points. Check if the distance in those points
		# is still up to data or whether there is a shorter path to them:
		for nID in curNeighbors:

			# Find distance between this neighbour and the current point:
			nPt = surface.GetPoint( nID )
			dist = math.sqrt( vtkMath.Distance2BetweenPoints( nPt, curPt ) )

			newDist = dist + curDist
			if newDist < distance.GetTuple1( nID ):
				distance.SetTuple1( nID, newDist )
				if not nID in front:
					front.append( nID )     # This neighbor node needs to be checked again!
		
	return distance

def generatePointNormals( surface ):
	# Check if the point normals already exist
	if surface.GetPointData().HasArray("Normals"):
		return surface

	# If no normals were found, generate them:
	normalGen = vtkPolyDataNormals()
	normalGen.SetInputData( surface )
	normalGen.ComputePointNormalsOn()
	normalGen.ComputeCellNormalsOff()
	normalGen.SplittingOff()        # Don't allow generator to add points at sharp edges
	normalGen.Update()
	return normalGen.GetOutput()

def getConnectedVertices( mesh, nodeID ):
	"""
	Find the neighbor vertices of the node with ID nodeID.

	Parameters
	----------
	mesh (vtkDataSet):
		The mesh topology
	nodeID (int):
		The ID of the node for which neighbors are to be found

	Returns
	----------
	list of int:
		IDs of the vertices that are neighbors of nodeID.

	"""
	connectedVertices = []

	#get all cells that vertex 'id' is a part of
	cellIDList = vtkIdList()
	mesh.GetPointCells( nodeID, cellIDList )
	
	for i in range(cellIDList.GetNumberOfIds()):
		c = mesh.GetCell( cellIDList.GetId(i) )
		pointIDList = vtkIdList()
		mesh.GetCellPoints( cellIDList.GetId(i), pointIDList )
		for j in range(pointIDList.GetNumberOfIds()):
			neighborID = pointIDList.GetId(j)
			if neighborID != nodeID:
				if not neighborID in connectedVertices:
					connectedVertices.append( neighborID )
	return connectedVertices

def removeDuplicatedIDs( curSpringIDs, prevSpringIDs ):
    """
    Removes indices of curSpringIDs that are already present in prevSpringIDs.

    Arguments:
    ----------
    curSpringIDs (list of ints):
        List of IDs where to remove indices.
    prevSpringIDs (list of ints):
        Reference list of IDs.
        
    Returns:
    ----------
    list of ints
        curSpringIDs where the IDs already present in prevSpringIDs have been removed.
    """
    # Input are two lists and removes from cur ind already rpesent in prev
    diffSpringIDs = curSpringIDs

    if prevSpringIDs:
        curSpringIDs = set(curSpringIDs)
        prevSpringIDs = set(prevSpringIDs)
        if len(curSpringIDs.intersection(prevSpringIDs)) > 0:
            diffSpringIDs = list(curSpringIDs.difference(prevSpringIDs))
    return diffSpringIDs

def getIndicesInROI( mesh, centerPoint, r, subset=None):
	"""
	Get the indices of mesh points belonging to a spherical ROI of radius r and
	centered in node centerID.

	Arguments:
	----------
	mesh (vtkDataSet, for example a vtkUnstructuredGrid):
		Mesh where the ROI is defined. centerID and subset (if defined) must come from this volume.
	centerPoint (list):
		3D coordinates of the ROI center.
	r (float):
		The radius (in meters) defining the ROI size. 
	subset (list of ids):
		List of IDs of mesh to consider in ROI selection. If not specified, all the 
		points in the mesh will be considered.
		
	Returns:
	----------
	list of ints
		IDs of the points in mesh falling at a distance < r from node centerID.
	"""
	sampledIDs = vtkIdList()  

	# mesh points
	pointsToSample = vtkPolyData()
	if subset is None:
		pointsToSample = utils.unstructuredGridToPolyData( mesh )
	else:
		points = vtkPoints()
		for i in subset:
			points.InsertNextPoint(mesh.GetPoint(i))
		pointsToSample.SetPoints(points)
	
	pointLocator = vtkPointLocator( )
	pointLocator.SetDataSet( pointsToSample )
	pointLocator.SetNumberOfPointsPerBucket(1)
	pointLocator.BuildLocator()
	pointLocator.FindPointsWithinRadius(r, centerPoint, sampledIDs)

	sampledIDlist = []
	for i in range(sampledIDs.GetNumberOfIds()):
		sampledIDlist.append(sampledIDs.GetId(i))

	return sampledIDlist

def randomSurface( fullSurface, wDistance=1, wNormal=1, wNoise=1, surfaceAmount=None, centerPointID=None ):
    """
    Extract a random part of a surface mesh. Starting from a (random) center node c, the
    surrounding points p are assigned three values:
        - Geodesic distance from the center node c
        - Angle difference between the normal of p and the normal of c
        - Random perlin noise value, sampled at the positon p.
    From these three values, we build a weighted sum, which acts as the likelyhood of a point
    being removed. We then select a threshold and remove all points whose likelyhood exceeds
    this threshold. The remaining points are the selected surface.
    The weights in the weighted sum can be used to influence whether the geodesic distance,
    the normals or the random perlin noise value should have a higher influence on the
    removal.

    Arguments:
    ----------
    fullSurface (vtkPolyData):
        Full surface. A part of this mesh will be returned. Point IDs and Cell IDs will not
        be kept.
    wDistance (float):
        Influence of the geodesic distance of a point to the center point c.
        If this value is > 0 and the other weights are == 0, only the distance will be
        taken into account.
    wNormal (float):
        Influece of the angle between normals.
        If this value is > 0 and the other weights are == 0, only points with a similar
        normal as the center point will be selected.only points with a similar
        normal as the center point will be selected.
    wNoise (float):
        Influence of the highest noise.
        If this value is > 0 and the other weights are == 0, entirely random parts of the
        surface will be selected.
    surfaceAmount (float):
        Amount of the surface which we want to select. If None, a random amount
        between 0 and 1 will be selected.
        Valid range: (0,1)
    centerPointID (int):
        Index of the center point c. If None, a random index of all the surface indices will be selected.

    Returns:
    ----------
    vtkPolyData
        Describes the part of the fullSurface which was selected
    """

    # Decide how many points we want to select:
    if surfaceAmount is None:
        surfaceAmount = random.random()
    assert surfaceAmount <= 1 and surfaceAmount > 0, "surfaceAmount must be between 0 and 1."

    pointsToSelect = surfaceAmount*fullSurface.GetNumberOfPoints()
    # print pointsToSelect

    fullSurface = generatePointNormals( fullSurface )
    normals = fullSurface.GetPointData().GetArray( "Normals" )

    # Select a random point on the surface around which to center the selected part, if it is not provided:
    if centerPointID is None:
        centerPointID = random.randint(0,fullSurface.GetNumberOfPoints()-1)
    centerPoint = fullSurface.GetPoint( centerPointID )

    distance = geodesicDistance( fullSurface, centerPointID )
    fullSurface.GetPointData().AddArray( distance )

    # Get normal of that point:
    centerPointNormal = normals.GetTuple3( centerPointID )


    # Decrease with:
    # - distance from center point
    # - normal difference
    # - perlin noise

    noise = vtkPerlinNoise()
    noise.SetFrequency( 15, 15, 15 )
    noise.SetPhase( random.random()*150, random.random()*150, random.random()*150 )

    # Create an array which will be filled and then used for thresholding
    likelyhood = vtkDoubleArray()
    likelyhood.SetNumberOfComponents(1)
    likelyhood.SetNumberOfTuples(fullSurface.GetNumberOfPoints())
    likelyhood.SetName("likelyhood")

    minVal = 99999
    maxVal = -1
    for i in range( fullSurface.GetNumberOfPoints() ):
        pt = fullSurface.GetPoint( i )
        dist = math.sqrt( vtkMath.Distance2BetweenPoints(centerPoint, pt) )

        normal = normals.GetTuple3( i )
        dot = vtkMath.Dot( centerPointNormal, normal )
        normalAng = math.acos( max( min( dot, 1 ), -1 ) )

        rnd = abs(noise.EvaluateFunction( pt ))

        curLikelyhood = wDistance*dist + wNormal*normalAng + wNoise*rnd
        likelyhood.SetTuple1( i, curLikelyhood )
        minVal = min( minVal, curLikelyhood )
        maxVal = max( maxVal, curLikelyhood )

    # print("Likelyhood range:", minVal, maxVal)

    # Build histogramm of likelyhoods:
    histBins = 50
    hist = [0]*histBins
    for i in range( fullSurface.GetNumberOfPoints() ):
        l = likelyhood.GetTuple1( i )
        curBin = int(l/maxVal*(histBins-1))
        hist[curBin] += 1
    # print hist
    # Find out where to set the threshold so that surfaceAmount points are selected.
    # We do this by going through the histogram and summing up the values in the bins. As
    # soon as more than surfaceAmount points are selected, 
    thresholdedPoints = 0
    threshold = maxVal  # Start with default of selecting everything
    for i in range( histBins ):
        thresholdedPoints += hist[i]
        if thresholdedPoints >= pointsToSelect:
            # print 'HERE, ', thresholdedPoints, pointsToSelect, i
            threshold = (i+1)/float(histBins)*maxVal
            # print threshold
            break
    # print("Selected threshold", threshold)

    fullSurface.GetPointData().AddArray( likelyhood )

    likelyhoodRange = maxVal - minVal

    #rndThreshold = (random.random()*0.75 + 0.25)*likelyhoodRange + minVal

    thresh = vtkThreshold()
    thresh.SetInputData( fullSurface )
    thresh.SetInputArrayToProcess( 0,0,0,
            vtkDataObject.FIELD_ASSOCIATION_POINTS, "likelyhood" )
    thresh.ThresholdBetween( 0, threshold )
    thresh.Update()

    # Write resulting surface to file:
    # Debug output:
    # writer = vtkXMLPolyDataWriter()
    # writer.SetFileName( "partialSurfaceLikelyhood.vtp" )
    # writer.SetInputData( fullSurface )
    # writer.Update()

    partialSurface = utils.unstructuredGridToPolyData( thresh.GetOutput() )

    # writer = vtkXMLPolyDataWriter()
    # writer.SetFileName( "partialSurface.vtp" )
    # writer.SetInputData( partialSurface )
    # writer.Update()

    #fullArea = surfaceArea( fullSurface )
    #partialArea = surfaceArea( partialSurface )

    #print( "Original area:", fullArea )
    #print( "Partial area:", partialArea )
    #print( "Selected amount: {:.2f}% (Target was {:.2f}%)".format( 100*partialArea/fullArea,
    #    100*surfaceAmount ) )

    return partialSurface

def gridRayCast( volume, numSteps=32 ):
    """
    Casts rays from below the volume. Returns list of mesh indices which were hit by a ray."

    Arguments:
    ----------
    volume (vtkPointSet, for example a vtkUnstructuredGrid):
        Organ volume. Rays will be shot from the -Y direction, assuming this is "below"
        the organ.
    numSteps (int):
        Number of steps along each side of the grid. Overall, numSteps^2 rays will be
        cast in a regular grid with side length numSteps. The grid is bounded by the 
        volume's bounding box.

    Returns:
    ---------- 
    list of int:
        The IDs of the volume nodes at which rays have hit. These can be used to set up
        springs.
    """

    volume = utils.unstructuredGridToPolyData( volume )

    # Locator for ray trace:
    obbTree = vtkOBBTree()
    obbTree.SetDataSet( volume )
    obbTree.BuildLocator()

    # Locator for nearby points:
    locator = vtkPointLocator( )
    locator.SetDataSet( volume )
    locator.SetNumberOfPointsPerBucket(1)
    locator.BuildLocator()

    bounds = volume.GetBounds()

    yMin = bounds[2]
    yMax = bounds[3]
    points = vtkPoints()
    cellIds = vtkIdList()

    springStartIDs = []
    # Iterate over the points in a regular grid:
    for step_x in range(numSteps+1):
        for step_z in range(numSteps+1):
            # Calculate grid coordinates. The grid should be bounded by the bounding-box,
            # i.e. the grid lies on the -Y plane of the bounding box.
            x = (bounds[1]-bounds[0])*step_x/numSteps + bounds[0]
            z = (bounds[5]-bounds[4])*step_z/numSteps + bounds[4]

            # Shoot ray "Upwards" towards the organ
            code = obbTree.IntersectWithLine((x, yMin, z), (x, yMax, z), points, cellIds)

            # If the ray hit...
            if points.GetNumberOfPoints() > 0:
                hitPoint = points.GetPoint(0)
                # Find the node closest to the hit:
                closestMeshPointID = locator.FindClosestPoint( hitPoint )
                closestMeshPointID = int(closestMeshPointID)
                # If the node has not been hit before, append it to the list of hit nodes
                if not closestMeshPointID in springStartIDs:
                    springStartIDs.append( closestMeshPointID )

    # Return the list of nodes which have been hit by a ray:
    return springStartIDs


#################################################################################
#
#                               MAIN FUNCTION
#
#################################################################################

def extractRandomFixed( volume_mesh, fixed_percentage_bounds=[0.05,0.45], seed=None, wDist_bounds=[1,4], wNormal_bounds=[1,2], wNoise_bounds=[0,0], exclude_pos=None, exclude_radius=0.01, num_rays=32 ):
    """
    Extract a random patch on the lowest surface of a volume mesh (where the lowest surface is defined by -Y direction).
    The shape of the extracted patch can be controlled by setting wDist_bounds, wNormal_bounds and wNoise_bounds. For a
    detailed description of how these parameters influence the shape, refer to randomSurface function.

    Arguments:
    ----------
    volume_mesh (str):
        Filename of the volume mesh to consider. A part of this mesh will be returned.
    fixed_percentage_bounds (list):
        A list containing the min and max percentage of the surface that can be extracted.
        Between 0 (0% of the surface can be extracted) and 1 (100% of the surface can be extracted). 
        A random value between these two bounds is selected each time.
    seed (int):
        Seed for random numbers generator. Can be specified to have deterministic results.
    wDist_bounds (list):
        A list containing the min and max values for the weight defining the influence of the geodesic distance of a 
        point during patch extraction.
    wNormal_bounds (list):
        A list containing the min and max values for the weight defining the influence of the angle between normals of
        points during patch extraction.
    wNoise_bounds (list):
        A list containing the min and max values for the weight defining the influence of noise during patch extraction.    
    exclude_pos (list):
        3D coordinates of a point which should be excluded from the extracted patch. However, exclusion of such point is not guaranteed.
    exclude_radius (float):
        if exclude_pos is specified, the center point for patch extraction is selected among the points farther than exclude_radius
        from exclude_pos
    num_rays (int):
        Number of rays used for raycasting, to extract the lowest surface. See gridRaycast for more details.
    Returns:
    ----------
    list of ints
        Indices of the extracted points
    """

    volume = utils.loadMesh( volume_mesh )
    surface = utils.extractSurface( volume )

    # Fix random seed
    if seed is not None:
        random.seed(seed)

    # Extract points towards -Y
    volumeLowestIDs = gridRayCast( volume, numSteps=num_rays ) #referred to volume indices
    candidateFixedIDs = []

    # Try to avoid extracting points too close to exclude_pos    
    if exclude_pos is not None:            
        excludeIDs = getIndicesInROI( volume, exclude_pos, exclude_radius )
        candidateFixedIDs = removeDuplicatedIDs( volumeLowestIDs, excludeIDs )

    if not len(candidateFixedIDs):
        candidateFixedIDs = volumeLowestIDs

    centerID        = candidateFixedIDs[random.randint(0, len(candidateFixedIDs)-1)] #centerID is a volume index
    surfaceCenterID = utils.getClosestPoints( volume, surface, [centerID] )[0] #corresponding surface index needed for extraction of surface patch

    curFixedIDs = []
    getOut = 0
    while(not len(curFixedIDs)): #stay here until at least some points are selected
        rndSurfaceAmount = np.abs(random.gauss(mu=0.01,sigma=0.04))
        rndSurfaceAmount = fixed_percentage_bounds[0] + rndSurfaceAmount * (fixed_percentage_bounds[1] - fixed_percentage_bounds[0])
        #rndSurfaceAmount  = random.uniform( fixed_percentage_bounds[0], fixed_percentage_bounds[1] )
        rndWeightDistance = random.uniform( wDist_bounds[0], wDist_bounds[1] )
        rndWeightNormal   = random.uniform( wNormal_bounds[0], wNormal_bounds[1] )
        rndWeightNoise    = random.uniform( wNoise_bounds[0], wNoise_bounds[1] )
        #print rndWeightDistance, rndWeightNoise, rndWeightNormal
        fixedSurface = randomSurface( surface,
                                    wDistance = rndWeightDistance,
                                    wNormal = rndWeightNormal,
                                    wNoise = rndWeightNoise,
                                    surfaceAmount = rndSurfaceAmount,
                                    centerPointID = surfaceCenterID )

        # Get volume indices corresponding to selected surface indices
        curFixedIDs = utils.getClosestPoints( fixedSurface, volume, discardDuplicate=True )
        getOut += 1
        if getOut > 10:
            raise IOError("Impossible to select fixed points!")
    
    return curFixedIDs