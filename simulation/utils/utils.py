from vtk import *
import math
import numpy as np
import yaml
import os
import sys
from numpy import linalg as LA
from scipy import spatial

# SOFA
def load_yaml(file):
	""" 
	Loads the YAML configuration file passed as command line argument when running Sofa.
	
	Arguments
	-----------
	file (str):
		path to yml file to load

	Returns
	-----------
	dict 
		containing the loaded data.
	"""
	
	with open(file, 'r') as stream:
		try:
			options = yaml.load(stream)
			# parse_file_paths( options, configFilePath )
			return options     

		except yaml.YAMLError as exc:
			print(exc)
			return

def get_mean_displacement(array1,array2):
	""" 
	Gets the average displacement between two arrays.

	Arguments
	-----------
	array1 : array_like
		Nx3 array with coordinates of N points.
	array2 : array_like
		Nx3 array with coordinates of N points.

	Returns
	-----------
	float
		Average displacement.
	"""
	assert len(array2)==len(array1)
	displacement = array2 - array1
	displ_norm = LA.norm(displacement, axis=1)
	return np.mean(displ_norm)

def get_distance_np( array1, array2 ):
	"""
	For each point in array2, returns the Euclidean distance with its closest point in array1 and
	the index of such closest point. 
	
	Arguments
	----------
	array1 (ndarray):
		N1 x M array with the points.
	array2 (ndarray):
		N2 x M array with the points.
	
	Returns
	----------
	dist:
		N2 x 1 Euclidean distance of each point of array2 with the closest point in array1.
		dist contains the same result as: 	np.nanmin( distance.cdist( array1, array2 ), axis=1 )
	indexes:
		N2 x 1 Indices of closest point in array1

	"""
	mytree = spatial.cKDTree(array1)
	dist, indexes = mytree.query(array2)
	return dist, indexes

def get_indices_in_bbox( positions, bbox ):
	"""
	Get the indices of the points falling within the specified bounding box.
	
	Arguments
	----------
	positions (list):
		N x 3 list of points coordinates.
	bbox (list):
		[xmin, ymin, zmin, xmax, ymax, zmax] extremes of the bounding box.
	
	Returns
	----------
	indices:
		List of indices of points enclosed in the bbox.

	"""
	# bbox is in the format (xmin, ymin, zmin, xmax, ...)
	assert len(bbox) == 6
	indices = []
	for i, x in enumerate( positions ):
		if x[0] >= bbox[0] and x[0] <= bbox[3] and x[1] >= bbox[1] and x[1] <= bbox[4] and x[2] >= bbox[2] and x[2] <= bbox[5]:
			indices.append( i )
	return indices

def normalize( values, min=0., max=1.0 ):
	new_values = (values - min) / (max - min)

	if isinstance(new_values, float):
		new_values = [new_values]
	else:
		new_values = new_values.tolist()

	if np.amax(new_values) > 1.0:
		normalized_values = [ i if i <= max else 1.0 for i in new_values ]
		new_values = normalized_values

	return new_values

def compute_visibility_closest_point( tissue_state, target_state, is_visible_threshold ):
	"""
	Get the an estimate of the visibility of the target, given the current tissue state.
	Visibility is computed as distance between the tissue point closest to the target, 
	normalized between 0 and is_visible_threshold.
	
	Arguments
	----------
	tissue_state (SOFA MechanicalObject):
		MechanicalObject of the deformable object.
	target_state (SOFA MechanicalObject):
		MechanicalObject of the target.
	is_visible_threshold (float):
		distance [m] associated to 100% visibility.
	
	Returns
	----------
	visible_perc:
		Estimate of visibility, computed as distance between the tissue point closest to the target, 
	normalized between 0 and is_visible_threshold.
	"""
	tissue_position = np.asarray( tissue_state.position )
	target_position = np.asarray( target_state.position )
	if not len(target_position) == 1:
		# If the target is not a single point, get the centroid
		target_position = np.mean( target_position, axis=0 )

	dist, idx = get_distance_np( tissue_position, target_position ) # returns only closest point to target
	visible_perc = normalize( dist[0], max=is_visible_threshold )
	return visible_perc[0]

def compute_visibility_region( tissue_state, target_state, is_visible_threshold, roi_indices=[] ):
	"""
	Get the an estimate of the visibility of the target, given the current tissue state. 
	Visibility is computed as number of tissue points whose distance from the target is above the 
	is_visible_threshold, over the total number of points in the considered region of interest.
	
	Arguments
	----------
	tissue_state (SOFA MechanicalObject):
		MechanicalObject of the deformable object.
	target_state (SOFA MechanicalObject):
		MechanicalObject of the target.
	is_visible_threshold (float):
		distance [m] to define visibility. If a tissue point is at distance > is_visible_threshold from the target,
		it is considered "far enough" from the target.
	roi_indices (list):
		indices of the tissue points to consider for computation of visibility. If not provided, all tissue points are considered.
	
	Returns
	----------
	visible_perc:
		Estimate of visibility, computed as number of tissue points whose distance from the target is above the 
		is_visible_threshold, over the total number of points in the considered region of interest.
	"""
	if not len(roi_indices):
		num_points = len(tissue_state.position)
		roi_indices = list(range(num_points))

	tissue_position = np.asarray( tissue_state.position )[roi_indices]
	target_position = np.asarray( target_state.position )
	if not len(target_position) == 1:
		# If the target is not a single point, get the centroid
		target_position = np.mean( target_position, axis=0 )

	dist, idx = get_distance_np( target_position, tissue_position )
	num_visible = len( [d for d in dist if d > is_visible_threshold] )
	visible_perc = float(num_visible) / len(roi_indices)
	return visible_perc

# UTILS
def apply_vtk_transform( mesh, transform ):
	tfFilter = vtkTransformFilter()
	tfFilter.SetTransform( transform )
	tfFilter.SetInputData( mesh )
	tfFilter.Update()
	return tfFilter.GetOutput()

# VTKUTILS
def loadMesh( filename ):
	"""
	Loads a mesh using VTK. Supported file types: stl, ply, obj, vtk, vtu, vtp, pcd.

	Arguments:
	---------
	filename (str)
	
	Returns:
	--------
	vtkDataSet
		which is a vtkUnstructuredGrid or vtkPolyData, depending on the file type of the mesh.
	"""

	# Load the input mesh:
	fileType = filename[-4:].lower()
	if fileType == ".stl":
		reader = vtkSTLReader()
		reader.SetFileName( filename )
		reader.Update()
		mesh = reader.GetOutput()
	elif fileType == ".obj":
		reader = vtkOBJReader()
		reader.SetFileName( filename )
		reader.Update() 
		mesh = reader.GetOutput()
	elif fileType == ".ply":
		reader = vtkPLYReader()
		reader.SetFileName( filename )
		reader.Update() 
		mesh = reader.GetOutput()
	elif fileType == ".vtk":
		reader = vtkUnstructuredGridReader()
		reader.SetFileName( filename )
		reader.Update() 
		mesh = reader.GetOutput()
	elif fileType == ".vtu":
		reader = vtkXMLUnstructuredGridReader()
		reader.SetFileName( filename )
		reader.Update() 
		mesh = reader.GetOutput()
	elif fileType == ".vtp":
		reader = vtkXMLPolyDataReader()
		reader.SetFileName( filename )
		reader.Update() 
		mesh = reader.GetOutput()
	elif fileType == ".pcd":
		print('DISABLED')
		exit()
		
	else:
		raise IOError("Mesh should be .vtk, .vtu, .vtp, .obj, .stl, .ply or .pcd file!")

	if mesh.GetNumberOfPoints() == 0:
		raise IOError("Could not load a valid mesh from {}".format(filename))
	return mesh

def unstructuredGridToPolyData( ug ):
	"""
	Converts an input unstructured grid into a polydata object. 
	Be careful since PolyData objects cannot contain 3D elements, thus all 
	tetrahedra will be lost with this operation.

	Parameters
	----------
	ug (vtkUnstructuredGrid):
		The input unstructured grid

	Returns
	----------
	vtkPolyData
	
	"""
	geometryFilter = vtkGeometryFilter()
	geometryFilter.SetInputData( ug )
	geometryFilter.Update()
	return geometryFilter.GetOutput()

def extractSurface( inputMesh ):
	surfaceFilter = vtkDataSetSurfaceFilter()
	surfaceFilter.SetInputData( inputMesh )
	surfaceFilter.Update()
	surface = surfaceFilter.GetOutput()
	return surface

def getClosestPoints( mesh1, mesh2, subset=None, discardDuplicate=False ):
	"""
	For each point in mesh1, returns the index of the closest point in mesh2. 
	
	Parameters
	----------
	mesh1 (vtkDataSet):
		Topology of the first mesh
	mesh2 (vtkDataSet):
		Topology of the second mesh
	subset (list of ints):
		If specified, a subset of mesh1 nodes are considered. Subset represents
		the list of node indices to consider.
		Default: None
	discardDuplicate (bool):
		If true, the returned indices cannot be present more than once.
		Default: False

	Returns
	----------
	list of int:
		IDs of mesh2 vertices closest to each mesh1 vertex.

	"""
	locator = vtkPointLocator( )
	locator.SetDataSet( mesh2 )
	locator.SetNumberOfPointsPerBucket(1)
	locator.BuildLocator()

	mesh2IDs = []
	if subset is None:
		subset = range(mesh1.GetNumberOfPoints())
		
	for idx in subset:
		mesh1Point = mesh1.GetPoint(idx)
		mesh2PointID = locator.FindClosestPoint( mesh1Point )
		# If we want to discard duplicate indices and we have already found it, skip append
		if discardDuplicate and (mesh2PointID in mesh2IDs):
			pass
		else:
			mesh2IDs.append( int(mesh2PointID) )
	return mesh2IDs
