# Simulation
This directory contains the simulation which exchanges information about tissue state with ROS. The folder is organized as follows:
- config/ contains configuration files for exchanging information to/from the simulation
- data/ contains 3d mesh models and other input data
- utils/ contains custom sofa components and utility functions

### Setup
Download and install SOFA (v20.06), following the instructions at https://www.sofa-framework.org/community/doc/getting-started/build/linux/. Simulation is implemented relying on SofaPython plugin, which needs to be manually activated in CMake (thus, python3 is not supported). Once built, SOFA can be launched with /PATH-TO-BUILD/bin/runSofa (you can either add runSofa to your ~/.bashrc or create a symlink). Required python modules for simulations can be found in simulation/requirements.txt.

Communication is implemented using ROS. ROS-related modules come with ROS distributions (tested with ROS melodic).

**Details**  
Simulation reads the parameters of interest (such as names of ros nodes and topics) from config/settings.yml file (custom config file can be provided by running `runSofa scene_sofa_ros_double.py --argv customconfig.yml`). 
Refer directly to the config file to check simulation settings that can be specified. In particular, it is possible to replicate scenarios A, B and C described in the paper and shown in the video.

***Simulation input*** 
- position_cartesian_current of both dvrk PSMs (i.e., 3D coordinates of end-effectors)
- state_jaw_current of both dvrk PSMs (that define gripper state: open/close)

***Simulation output*** 
- point cloud with the position of all tissue points (N points)
- force vector associated to each point (a flattened array of dimension Nx3)
- point cloud with the position of the attachment points
- float value representing the estimate of target visibility
