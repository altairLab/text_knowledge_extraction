"""
Forward dynamic simulation using both PSMs. 
PSM motion is driven by ROS.
Tissue grasping is triggered by ROS; tissue points within a certain distance from the EE are grasped (if any).
To launch this scene: runSofa scene_sofa_ros_double.py 
Default config file loaded: ./config/settings.yml
"""
import sys, os
import numpy as np
import math, random
import time, copy

# SIMULATION
import Sofa
from vtk import *
sys.path.append('utils')
import utils
from extract_random_patch import extractRandomFixed
sys.path.append('utils/components')
from tissue import Tissue, Material
from solver import Solver
from boundaries import FixedBoundaries
from ee import EE
from target import Target

config_file = './config/settings.yml'
# if len(sys.argv) > 1:
# 	config_file = sys.argv[1]
options = utils.load_yaml(config_file)
print "Loaded config from ", config_file

abs_path = os.path.dirname(os.path.abspath(__file__))

##################################
####### SETUP ROS
##################################
import rospy
from std_msgs.msg import Float32, Float32MultiArray, Header, Bool
from sensor_msgs import point_cloud2
from sensor_msgs.msg import PointCloud2, PointField, JointState
from geometry_msgs.msg import PoseStamped
import tf, struct

fields = [
	PointField('x', 0, PointField.FLOAT32, 1),
	PointField('y', 4, PointField.FLOAT32, 1),
	PointField('z', 8, PointField.FLOAT32, 1),
	PointField('rgba', 12, PointField.UINT32, 1),
	]
rgba_tissue = struct.unpack('I', struct.pack('BBBB', 255, 0, 255, 255))[0]
rgba_fixed  = struct.unpack('I', struct.pack('BBBB', 255, 0, 0, 255))[0]

# Init SOFA node
rospy.init_node(options['ros']['sofa_node'])

tf_world2sofa = options['registration']['world2sofa']
tf_world2sofa = np.asarray(tf_world2sofa).reshape((4,4))
tf_sofa2world = np.linalg.inv(tf_world2sofa)

psm1_pose = None
psm2_pose = None
tf_list = tf.TransformListener()

psm1_grasp = False
psm2_grasp = False

reset = False

# Callback for PSM1 and PSM2 poses
def psm1_callback(msg):
	global psm1_pose
	global tf_list
	psm1_base = msg
	psm1_base.header.frame_id = "PSM1_base"
	psm1_base.header.stamp = rospy.Time(0.)
	tf_list.waitForTransform("world", "PSM1_base", time=rospy.Time(0.), timeout = rospy.Duration(5.))
	psm1_world = tf_list.transformPose("world", psm1_base)

	rcv_pose  = [psm1_world.pose.position.x, psm1_world.pose.position.y, psm1_world.pose.position.z, 1]
	psm1_pose = list(tf_world2sofa.dot(np.asarray(rcv_pose)) )[:-1]
	
def psm2_callback(msg):
	global psm2_pose
	global tf_list
	psm2_base = msg
	psm2_base.header.frame_id = "PSM2_base"
	psm2_base.header.stamp = rospy.Time(0.)
	tf_list.waitForTransform("world", "PSM2_base", time=rospy.Time(0.), timeout = rospy.Duration(5.))
	psm2_world = tf_list.transformPose("world", psm2_base)

	rcv_pose  = [psm2_world.pose.position.x, psm2_world.pose.position.y, psm2_world.pose.position.z, 1]
	psm2_pose = list(tf_world2sofa.dot(np.asarray(rcv_pose)) )[:-1]

# Callback triggering grasping
def psm1_grasp_callback(msg):
	global psm1_grasp
	grasp_state = msg.position[0]
	if grasp_state < 0:
		psm1_grasp = True
	else:
		psm1_grasp = False

def psm2_grasp_callback(msg):
	global psm2_grasp
	grasp_state = msg.position[0]
	if grasp_state < 0:
		psm2_grasp = True
	else:
		psm2_grasp = False

def reset_callback(msg):
	global reset
	reset = True

# Subscriber for PSM1 and PSM2
sub_psm1 = rospy.Subscriber(options['ros']['psm1_pose_topic'], PoseStamped, psm1_callback, queue_size=1)
sub_psm2 = rospy.Subscriber(options['ros']['psm2_pose_topic'], PoseStamped, psm2_callback, queue_size=1)
sub_psm1_grasp = rospy.Subscriber(options['ros']['psm1_jaw_topic'], JointState, psm1_grasp_callback, queue_size=1)
sub_psm2_grasp = rospy.Subscriber(options['ros']['psm2_jaw_topic'], JointState, psm2_grasp_callback, queue_size=1)

# Publishers from SOFA
pub_tissue  = rospy.Publisher(options['ros']['sofa_tissue_topic'], PointCloud2, queue_size=1)
pub_forces  = rospy.Publisher(options['ros']['sofa_forces_topic'], Float32MultiArray, queue_size=1)
pub_fixed   = rospy.Publisher(options['ros']['sofa_fixed_topic'], PointCloud2, queue_size=1)
pub_visible = rospy.Publisher(options['ros']['visible_perc_topic'], Float32, queue_size=1)
pub_target  = rospy.Publisher(options['ros']['target_position_topic'], Float32MultiArray, queue_size=1)

##################################
####### SETUP SIMULATION
##################################

def createScene(rootNode):
	print("Python version:", sys.version) 
	rootNode.createObject('RequiredPlugin',name='SofaPython',pluginName='SofaPython')
	rootNode.createObject('BackgroundSetting', color='1 1 1 1')

	print("Waiting to receive the first pose for PSM1...")
	while True:
		if psm1_pose is not None:
			psm1_init = psm1_pose
			Simulation(rootNode, psm1_init)
			return
		else:
			rospy.sleep(0.1)

class Simulation(Sofa.PythonScriptController):

	def __init__(self, rootNode, psm1_init_pose):
		super(Simulation,self).__init__()

		self.tissue = None
		self.psm1_init_pose = psm1_init_pose
		self.rootNode = rootNode
		
		self.unstable_pub = rospy.Publisher("/unstable_test", Bool, queue_size=1)

		# Global sim parameters
		rootNode.findData('dt').value = options['simulation']['dt']
		rootNode.findData('gravity').value = options['simulation']['gravity']

		# Visualization
		self.view_mode = options['simulation']['view_mode']
		if not self.view_mode:
			# Start animation if you are not visualizing
			rootNode.findData('animate').value = True
			rootNode.createObject('VisualStyle', displayFlags='showBehaviorModels showForceFields')
		else:
			rootNode.findData('animate').value = options['simulation']['animate']
			rootNode.createObject('VisualStyle', displayFlags='hideBehaviorModels hideForceFields showCollisionModels showWireframe')
			rootNode.createObject('LightManager')
			rootNode.createObject('DirectionalLight', name="light1", color="1 1 1", direction="-0.7 0.7 0") 
			rootNode.createObject('DirectionalLight', name="light2", color="1 1 1", direction="0.7 -0.7 0") 
			
		# Specifications 
		self.random_target = options["simulation"]["random_target"]
		self.random_fixed  = options["simulation"]["random_fixed"]

		scenarios   = ["A","B","C"]
		seed_fixed  = [1400,100,400]      		  
		pos_target =  [ [0.030, -0.0025, -0.056] , [-0.0, -0.0015, 0.006], [-0.0, -0.0015, -0.005] ] 

		if not(self.random_fixed) and not(self.random_target):
			assert options["simulation"]["scenario"] in scenarios, "The specified deterministic scenario does not exist"
			i = scenarios.index(options["simulation"]["scenario"])
			print '**** Simulation for scenario ', options["simulation"]["scenario"]
			self.seed_fixed  	 = seed_fixed[i]
			self.target_position = pos_target[i]
			self.ks = 0.01
			self.kd = 0.01
		else:
			self.seed_fixed 	 = 420
			self.target_position = [0,0,0]
			self.ks = 0.0
			self.kd = 0.0

		self.createGraph(rootNode)
		return None


	def createGraph(self,rootNode):
		
		# Mesh
		datadir    = options['filenames']['datadir']
		undeformed_surface_mesh = os.path.join(datadir, options['filenames']['surface_mesh'])
		undeformed_tetra_mesh   = os.path.join(datadir, options['filenames']['tetra_mesh'])
		self.undeformed_tetra_mesh = undeformed_tetra_mesh
		ee_surface_mesh = os.path.join(datadir, options['filenames']['ee_mesh'])
		ee2_surface_mesh = os.path.join(datadir, options['filenames']['ee2_mesh'])
		target_surface_mesh = os.path.join(datadir, options['filenames']['target_mesh'])
		plane_mesh = os.path.join(datadir, options['filenames']['floor_mesh'])
					
		# Tissue
		material = Material(
			young_modulus=options['simulation']['model']['E'],
			poisson_ratio=options['simulation']['model']['nu'],
			constitutive_model=options['simulation']['model']['material'],
			mass_density=options['simulation']['model']['rho'],
		)

		tissue = Tissue(
				rootNode,
				surface_mesh=undeformed_surface_mesh,
				tetra_mesh=undeformed_tetra_mesh,
				material=material,
				name='Tissue',
				view=True
			)
		self.tissue = tissue
		self.visible_roi_indices = []

		if self.view_mode:
			color_lines = [0.8,0.3,0.1,0.4]
			coll4view = tissue.node.createChild('Coll4ViewNode')
			coll4view.createObject('MeshTopology', src='@../surface_mesh', name='surface')
			coll4view.createObject('MechanicalObject', src='@../surface_mesh', name='Coll4ViewState')        
			coll4view.createObject('BarycentricMapping')
			coll4view.createObject('LineCollisionModel', color=color_lines)
		
		#####################################
		# Fixed points
		if self.random_fixed:
			# If we want the bbox (decomment extractRandomFixed):
			fixed_bbox = [-0.06,-0.01,-0.07,np.random.uniform(-0.05, 0.),0.01,np.random.uniform(-0.06, 0.07)]
			fixed_indices = utils.get_indices_in_bbox( self.tissue.tetra_mesh_topology.position, fixed_bbox )

		# seed_fixed = self.seed_fixed
		# if self.random_fixed:
		# 	seed_fixed = None
	
		# undeformed_tetra_mesh_abs = os.path.join( abs_path, undeformed_tetra_mesh )
		# fixed_indices = extractRandomFixed(undeformed_tetra_mesh_abs,seed=seed_fixed,fixed_percentage_bounds=[0.05, 0.05]) # for normal executions
		# fixed_indices = extractRandomFixed(undeformed_tetra_mesh_abs,seed=seed_fixed,fixed_percentage_bounds=[0.9, 0.9]) # for easier simulation of grasping fixed points
		# fixed_indices = extractRandomFixed(undeformed_tetra_mesh_abs,seed=seed_fixed)

		fixed_positions = [ self.tissue.tetra_mesh_topology.position[i] for i in fixed_indices ]
		print 'Number of fixed points: ', len(fixed_indices)
			
		self.boundaries = FixedBoundaries(
				parent_node=tissue.node,
				indices=fixed_indices,
				view=self.view_mode
			)
		self.fixed_indices = fixed_indices

		if self.view_mode:
			color_fixed = [0, 0, 1, 1]
			visual_fixed_node = tissue.node.createChild('VisualFixed')
			visual_fixed_node.createObject('MechanicalObject', position=fixed_positions, showObject=1, showObjectScale=12, showColor=color_fixed)

		########################################################################
		# Solver
		solver_name = 'Solver1'
		solver_type = options['simulation']['solver']
		#solver = Solver(parent_node=tissue.node, analysis=Solver.STATIC, solver=solver_type, solver_name=solver_name, num_iter=1000)
		#solver = Solver(parent_node=tissue.node, analysis=Solver.DYNAMIC, solver=solver_type, solver_name=solver_name, num_iter=1000)
		solver = Solver(parent_node=tissue.node, analysis=Solver.DYNAMIC, solver=Solver.CG, ks=self.ks, kd=self.kd, solver_name=solver_name, num_iter=1000)
		tissue.node.createObject('MeshMatrixMass', massDensity=material.mass_density, name='myMatrixMass')

		########################################################################
		# Psm
		grasp_distance 	   = options['simulation']['grasp_distance']
		num_grasped_points = options['simulation']['num_grasped_points']

		psm1 = EE(rootNode, 
				tissue, 
				name='PSM1', 
				num_grasp_points=num_grasped_points, 
				grasp_distance=grasp_distance
			)

		psm1.set_position( self.psm1_init_pose )
		if self.view_mode:
			psm1.visualize(ee_surface_mesh)
		self.psm1 = psm1

		psm2 = EE(rootNode, 
				tissue, 
				name='PSM2', 
				num_grasp_points=num_grasped_points, 
				grasp_distance=grasp_distance
			)

		self.psm2_init_pose = self.psm1_init_pose
		self.psm2_init_pose[0] += 0.02
		psm2.set_position( self.psm2_init_pose )
		if self.view_mode:
			psm2.visualize(ee2_surface_mesh)
		self.psm2 = psm2


		########################################################################
		# Target
		target = Target(rootNode, 
						surface_mesh=target_surface_mesh,
						view=True	
					)
		target_bounds = tissue.bounding_box
		target_bounds[4] = target_bounds[1]
		
		# Init position
		if self.random_target:
			self.target_position = [np.random.uniform(0., 0.04), -0.0025, np.random.uniform(-0.05, 0.05)]

		target.set_position( self.target_position )


			# seed_target = None
			# max_trials  = 50
		
			# # Try to place the target far from attachments for max_trials times
			# for k in range(max_trials): 
			# 	print "SEED ", seed_target
			# 	self.target_position = target.set_rnd_position(bounds=target_bounds, seed=seed_target)
			# 	print "Target position", self.target_position
			# 	sys.stdout.flush()

			# 	dist_target_fixed, __ = utils.get_distance_np( fixed_positions, self.target_position )
			# 	if dist_target_fixed > 0.01:
			# 		print 'Distance target-fixed points ', dist_target_fixed
			# 		break
			# 	seed_target = time.time() # next time, try new seed
			#assert k != max_trials-1, "Impossible to find random point far enough from the attachments"
		
		self.target = target
		
		# Initialize root bbox for visualization (with qglviewer interface)
		rootNode.bbox = tissue.bounding_box


		# Collision model for the base plane
		if self.view_mode:
			color_plane = [0.6,0.7,0.7,1]
			plane = rootNode.createChild('Plane')
			plane.createObject('MeshSTLLoader', name='planeLoader', filename=plane_mesh)
			plane.createObject('MeshTopology', src='@planeLoader', name='planeTopology')
			plane.createObject('TriangleSetTopologyContainer', name='planeTopology', src='@planeLoader')
			plane.createObject('OglModel', color=color_plane)
		

	def bwdInitGraph(self, rootNode):
		self.start_time = time.time()

		self.visible_roi_indices = self.tissue.compute_spherical_roi( self.target.state.position, 0.01 )

		# Publish tissue state at rest
		print 'Sending initial tissue state...'
		publish_pointcloud2( self.tissue.state.position, pub_tissue, rgba_tissue )
		pub_visible.publish(Float32(0.0))

	def onBeginAnimationStep(self,dt):

		# Setting received pose at each time step
		self.psm1.set_position( psm1_pose )
		self.psm2.set_position( psm2_pose )

		# Setting grasping state at each time step
		self.psm1.has_grasped = psm1_grasp
		self.psm2.has_grasped = psm2_grasp

	def onEndAnimationStep(self,dt):

		# Check for simulation instability at the end of each time step
		# current_pos = np.array(self.tissue.state.position)

		if not(self.psm1.has_grasped) and not(self.psm2.has_grasped):	
			self.reset()
			global grasp_pose
			grasp_pose = None

		if self.tissue.is_stable is False:
			print("Simulation is UNSTABLE, please decrease motion velocity.")
			self.unstable_pub.publish(Bool(True))
		
		#print 'publishing tissue state'

		# Publish tissue state
		publish_pointcloud2( self.tissue.state.position, pub_tissue, rgba_tissue )

		# Publish forces
		# vonMisesPerElement = np.asarray(self.tissue.ff.vonMisesPerElement)
		# vonMisesPerNode = np.asarray(self.tissue.ff.vonMisesPerNode)
		forces = [ f for sublist in self.tissue.state.force for f in sublist ]
		#rospy.loginfo(max(np.linalg.norm( np.asarray(self.tissue.state.force), axis=1 ))) # if we want to publish norm(force) per node
		msg_forces = Float32MultiArray(data=forces)
		pub_forces.publish(msg_forces)

		# Publish fixed points
		fixed_points = [ self.tissue.state.position[i] for i in self.fixed_indices ]
		publish_pointcloud2( fixed_points, pub_fixed, rgba_fixed )

		# Publish alias for visible percentage
		visible_perc = utils.compute_visibility_region( self.tissue.state, self.target.state, options["simulation"]["isvisible_distance"], roi_indices=self.visible_roi_indices )
		#print "Visible %: ", visible_perc*100
		pub_visible.publish(Float32(visible_perc*100))

		# Publish target position
		target_position = position2dvrk(self.target.state.position)
		msg_target = Float32MultiArray(data=target_position[0])
		pub_target.publish(msg_target)
		
	def reset(self):
		self.tissue.reset()
		# Create new target position
		sys.stdout.flush()
		return 0


def publish_pointcloud2(positions, topic, rgba):
	points = []
	for x,y,z in positions:
		x,y,z,__ = list(tf_sofa2world.dot(np.asarray([x,y,z,1])))
		points.append([x,y,z,rgba])
	
	header = Header()
	header.frame_id = options['ros']['pcl_frame_id']
	header.stamp = rospy.Time.now()
	pcl_msg = point_cloud2.create_cloud(header, fields, points)
	topic.publish(pcl_msg)

def position2dvrk(positions):
	points = []
	for x,y,z in positions:
		x,y,z,__ = list(tf_sofa2world.dot(np.asarray([x,y,z,1])))
		points.append([x,y,z])
	return points