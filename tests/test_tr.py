#!/usr/bin/env python2

import copy
import rospkg
import rospy
from std_msgs.msg import Bool, Empty, Float32
import os
import sys
import time
import random
import subprocess
from dvrk_task_msgs.msg import ActionArray, ContextModel
path_pkg = rospkg.RosPack().get_path("task_reasoning")
sys.path.append(path_pkg + "/scripts/")
from gringoParser import string2fun as parse
from clingo import Function as Fun, Control

import numpy as np
from matplotlib import pyplot as plt







class Listener(object):
    def __init__(self):
        self.path_pkg = rospkg.RosPack().get_path("robot_control")
        self.path_sofa = "runSofa"
        self.path_sim = os.path.join(self.path_pkg, '../simulation/scene_sofa_ros_double.py')
        self.launch_files = ["framework_tr", "framework_tr_text"]
        self.visible = 0.
        self.distance = 0.
        self.actions = []
        self.ended = False
        self.unstable = False
        self.context = ContextModel()
        self.power_on_pub = rospy.Publisher("/dvrk/console/power_on", Empty, queue_size=1)
        self.power_off_pub = rospy.Publisher("/dvrk/console/power_off", Empty, queue_size=1)
        self.home_pub = rospy.Publisher("/dvrk/console/home", Empty, queue_size=1)

        self.visible_listener = rospy.Subscriber("/target_visible_perc", Float32, self.visible_cb)
        self.context_listener = rospy.Subscriber("/context/model", ContextModel, self.context_cb)
        self.ended_listener = rospy.Subscriber("/ended_test", Bool, self.ended_cb)
        self.unstable_listener = rospy.Subscriber("/unstable_test", Bool, self.unstable_cb)
        self.actions_listener = rospy.Subscriber("/action_test", ActionArray, self.action_cb)
    
    def ended_cb(self, data):
        self.ended = data.data

    def unstable_cb(self, data):
        self.unstable = data.data
    
    def visible_cb(self, data):
        self.visible = data.data

    def action_cb(self, data):
        self.actions = data

    def context_cb(self, data):
        self.context = data





  





def test_sofa(listener):    
    num_tests = 100
    visible_list = []
    actions_list = []
    context_list = []
    visible_list_text = []
    actions_list_text = []
    context_list_text = []

    for i in range(num_tests):
        #start sofa
        proc = subprocess.Popen([listener.path_sofa, listener.path_sim, "-g", "batch", "--nbIter", "100000000"])
        rospy.sleep(2.)

        for j in range(len(listener.launch_files)):
            #start dvrk
            listener.power_on_pub.publish(Empty())
            rospy.sleep(5.)
            listener.home_pub.publish(Empty())
            rospy.sleep(5.)
            
            #reset variables
            listener.visible = 0.
            listener.actions = ActionArray()
            listener.context = ContextModel()
            listener.ended = False
            listener.unstable = False

            proc1 = subprocess.Popen(["roslaunch", listener.path_pkg + "/launch/" + listener.launch_files[j] + ".launch"])

            while not listener.ended and not listener.unstable:
                pass
                
            if listener.unstable:
                i = max(0, i-1)
                j = j+1

            elif listener.ended:
                rospy.sleep(5.) #ALLOW PUBLICATION OF DATA FROM SENSING
                out_file = listener.path_pkg + "/../tests/" + listener.launch_files[j]
                if "text" not in out_file:
                    visible_list.append(listener.visible)
                    actions_list.append(listener.actions)
                    context_list.append(listener.context)

                    #WRITE DATA FOR SCENARIO
                    visible = np.array(visible_list)
                    actions = np.array(actions_list)
                    context = np.array(context_list)
                else:
                    visible_list_text.append(listener.visible)
                    actions_list_text.append(listener.actions)
                    context_list_text.append(listener.context)

                    #WRITE DATA FOR SCENARIO
                    visible = np.array(visible_list_text)
                    actions = np.array(actions_list_text)
                    context = np.array(context_list_text)

                np.savez(out_file, visible=visible, actions=actions, context=context) #label=value
                
            #SWITCH OFF CONSOLE
            listener.power_off_pub.publish(Empty())
            rospy.sleep(5.)

            #KILL ROS NODES
            try:
                os.system("rosnode kill ASPplanner")
                os.system("ps -ef | grep 'ASP_manager' | grep -v grep | awk '{print $2}' | xargs -r kill -9")
            except:
                rospy.loginfo("ALREADY KILLED ASPplanner")
            try:
                os.system("rosnode kill sensing_node")
                os.system("ps -ef | grep 'sensing' | grep -v grep | awk '{print $2}' | xargs -r kill -9")
            except:
                rospy.loginfo("ALREADY KILLED sensing")
            try:
                os.system("rosnode kill control_node")
                os.system("ps -ef | grep 'control.py' | grep -v grep | awk '{print $2}' | xargs -r kill -9")       
            except:
                rospy.loginfo("ALREADY KILLED control")
            proc1.kill()
            proc1.wait()

        #TERMINATE SOFA
        os.system("ps -ef | grep 'sofa' | grep -v grep | awk '{print $2}' | xargs -r kill -9") #dvrk
        proc.kill()
        proc.wait()

        rospy.sleep(30.)

    #TERMINATE DVRK
    os.system("ps -ef | grep 'control' | grep -v grep | awk '{print $2}' | xargs -r kill -9") #dvrk 














class Solver(object):    
    def __init__(self, asp_name, path_pkg, grid_size):
        self.control = Control(["-c n=" + str(grid_size)])
        self.control.load(path_pkg + "/asp/" + asp_name)
        self.filename = asp_name
        self.control.ground([("base", [])])        
        self.atoms = []
        self.hidden_atoms = []
        self.ordered_atoms = []
        self.action_time = 0  
        self.step = 1 #for iterative solver
        self.max_time_sec = 200.
        

    def on_model(self, model):
        self.atoms[:] = model.symbols(shown=True)
        self.hidden_atoms[:] = model.symbols(atoms=True)

    def solve(self, context):
        #GROUND NEW EXTERNALS
        for atom in context:
            self.control.assign_external(parse(atom), True)

        init_time = time.time()
        while time.time() - init_time < self.max_time_sec:
            parts = []

            self.control.cleanup()
            parts.append(("check", [self.step]))
            self.control.release_external(Fun("query", [self.step-1]))

            parts.append(("step", [self.step]))

            self.control.ground(parts)
            self.control.assign_external(Fun("query", [self.step]), True)
            result = self.control.solve(on_model = self.on_model)
            self.step += 1
            if result.satisfiable:
                break
        compute_time = time.time() - init_time

        if result.satisfiable:
            if self.atoms != []:
                for atom in self.atoms:
                    tmp_list = ['none', 'none', 'none', 'none', 0]
                    tmp_list[0] = atom.name # name...
                    for i in range(len(atom.arguments)-1): # ...arguments...
                        tmp_list[i+1] = str(atom.arguments[i])
                    if not "text" in self.filename:
                        tmp_list[-1] = int(str(atom.arguments[-1])) + self.action_time - 1 # ...time
                    else:
                        tmp_list[-1] = int(str(atom.arguments[-1])) # ...time
                    if tmp_list not in self.ordered_atoms:
                        self.ordered_atoms.append(tmp_list)

                self.ordered_atoms.sort(key = lambda action: action[-1])
        
        return compute_time, self.ordered_atoms












def test_asp():
    path_pkg = rospkg.RosPack().get_path("task_reasoning")
    asp_names = ["tissue_retraction", "tissue_retraction_text"]
    psms = ["psm1", "psm2"]
    grid_sizes = [5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
    num_tests = 100

    for size in grid_sizes:
        plan_list = []
        time_list = []
        plan_list_text = []
        time_list_text = []
        context_list = []
        for _ in range(num_tests):
            context = []

            num_fixed = np.random.randint(1, np.floor(size*size/2))
            rows = range(size)
            row_start = random.choice(rows)
            row = row_start
            col = 0
            invert = False
            while len(context) < num_fixed:
                if col in range(size):
                    context.append("fixed((" + str(row+1) + "," + str(col+1) + "))")
                    col = col+1
                else:
                    if invert:
                        row = row-1
                    elif row < size-1:
                        row = row+1
                    else:
                        row = row_start-1
                        invert = True
                    col = 0

            for row in range(size): 
                for col in range(size):
                    closest_psm = random.choice(psms)
                    context.append("distance(" + closest_psm + ",(" + str(row+1) + "," + str(col+1) + "),1)")
                    context.append("distance(" + [psm for psm in psms if psm != closest_psm][0] + ",(" + str(row+1) + "," + str(col+1) + "),2)")

            context_list.append(context)
            for asp in asp_names:
                times = copy.deepcopy([])
                time_median = 0.
                out_file = path_pkg + "/../tests/" + asp + "_" + str(size)
                out_file.replace("_test", "")
                solver = Solver(asp + ".lp", path_pkg, size)
                if "text" not in asp:
                    context += ["closed_gripper(psm1)", "closed_gripper(psm2)"]
                else:
                    context = context_list[-1]

                for _ in range(num_tests/5):
                    solver = Solver(asp + ".lp", path_pkg, size)
                    time,plan = solver.solve(context)
                    times.append(time)

                time_median = np.median(times)
                if "text" in asp:
                    time_list_text.append(time_median)
                    plan_list_text.append(plan)
                    time = np.array(time_list_text)
                    plan = np.array(plan_list_text)
                else:
                    time_list.append(time_median)
                    plan_list.append(plan)
                    time = np.array(time_list)
                    plan = np.array(plan_list)

                np.savez(out_file, plan=plan, time=time, context=np.array(context_list)) #label=value
            






def read():
    grid_sizes = np.linspace(5, 15, 11)
    time_list_text = []
    time_list = []
    ratio_list = []
    for size in grid_sizes:
        #READ
        file  = np.load("tissue_retraction_" + str(int(size)) + ".npz")
        times = file["time"]
        plans = file["plan"]

        file = np.load("tissue_retraction_text_" + str(int(size)) + ".npz")
        times_text = file["time"]
        plans_text = file["plan"]

        time_list_text.append((np.mean(times_text), np.std(times_text)))
        time_list.append((np.mean(times), np.std(times)))

        ratio_list.append((np.mean(np.divide(times_text, times)), np.std(np.divide(times_text, times))))

    #PLOT PLANNING TIME
    # plt.figure()
    # plt.errorbar(grid_sizes, [time[0] for time in time_list], yerr=[time[1] for time in time_list], fmt='-o', elinewidth=3, markeredgewidth=3, capsize=5, label="Hand-written")
    # plt.errorbar(grid_sizes, [time[0] for time in time_list_text], yerr=[time[1] for time in time_list_text], fmt='-o', elinewidth=3, markeredgewidth=3, capsize=5, label="Text")
    # plt.legend(loc="upper left", fontsize=35)
    # plt.xlabel("Grid size", fontsize=35)
    # plt.ylabel("Planning time [s]", fontsize=35)
    # plt.xlim(left=grid_sizes[0])
    # plt.xticks(fontsize=35)
    # plt.yticks(fontsize=35)
    # plt.grid(linewidth=3)
    # plt.show()

    #PLOT RATIO
    print(np.mean([t[0] for t in ratio_list]))
    plt.figure()
    plt.errorbar(grid_sizes, [ratio[0] for ratio in ratio_list], yerr=[ratio[1] for ratio in ratio_list], fmt='-o', elinewidth=3, markeredgewidth=3, capsize=5)
    plt.xlabel("Grid size", fontsize=35)
    plt.ylabel("Ratio", fontsize=35)
    plt.xlim(left=grid_sizes[0])
    plt.xticks(fontsize=35)
    plt.yticks(fontsize=35)
    plt.grid(linewidth=3)
    plt.show()









    





if __name__ == '__main__':
    # rospy.init_node("test_tr")
    # listener = Listener()
    # rospy.sleep(1.)

    # test_sofa(listener)
    # test_asp()
    read()

    # rospy.spin()