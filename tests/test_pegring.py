#!/usr/bin/env python

from numpy.core.defchararray import count
from numpy.core.fromnumeric import sort
from clingo import Function as Fun, Control
import time
import csv
import sys
import rospkg
path_pkg = rospkg.RosPack().get_path("task_reasoning")
sys.path.append(path_pkg + "/scripts/")
from gringoParser import string2fun as parse
from matplotlib import pyplot as plt
import random
import copy

import numpy as np










class Solver(object):    
    def __init__(self, asp_name, path_pkg):
        self.control = Control([])
        self.control.load(path_pkg + "/asp/" + asp_name)
        self.filename = asp_name
        self.control.ground([("base", [])])        
        self.atoms = []
        self.hidden_atoms = []
        self.ordered_atoms = []
        self.action_time = 0  
        self.step = 1 #for iterative solver
        self.max_time_sec = 200.
        

    def on_model(self, model):
        self.atoms[:] = model.symbols(shown=True)
        self.hidden_atoms[:] = model.symbols(atoms=True)

    def solve(self, context):
        #GROUND NEW EXTERNALS
        for atom in context:
            self.control.assign_external(parse(atom), True)

        init_time = time.time()
        while time.time() - init_time < self.max_time_sec:
            parts = []

            self.control.cleanup()
            parts.append(("check", [self.step]))
            self.control.release_external(Fun("query", [self.step-1]))

            parts.append(("step", [self.step]))

            self.control.ground(parts)
            self.control.assign_external(Fun("query", [self.step]), True)
            result = self.control.solve(on_model = self.on_model)
            self.step += 1
            if result.satisfiable:
                break
        compute_time = time.time() - init_time

        if result.satisfiable:
            if self.atoms != []:
                for atom in self.atoms:
                    tmp_list = ['none', 'none', 'none', 'none', 0]
                    tmp_list[0] = atom.name # name...
                    for i in range(len(atom.arguments)-1): # ...arguments...
                        tmp_list[i+1] = str(atom.arguments[i])
                    if not "text" in self.filename:
                        tmp_list[-1] = int(str(atom.arguments[-1])) + self.action_time - 1 # ...time
                    else:
                        tmp_list[-1] = int(str(atom.arguments[-1])) # ...time
                    if tmp_list not in self.ordered_atoms:
                        self.ordered_atoms.append(tmp_list)

                self.ordered_atoms.sort(key = lambda action: action[-1])

        return compute_time, self.ordered_atoms












def test():
    plan_list = []
    time_list = []
    plan_list_text = []
    time_list_text = []
    context_list = []
    path_pkg = rospkg.RosPack().get_path("task_reasoning")
    asp_names = ["peg_ring", "peg_ring_text"]
    rings = ['red', 'green', 'blue', 'yellow']
    psms = ["psm1", "psm2"]
    num_tests = 100

    for _ in range(num_tests):
        context = copy.deepcopy([])

        #select visible rings
        num_rings = random.choice([1, 2, 3, 4])
        selected = []
        for i in range(num_rings):
            left = [ring for ring in rings if ring not in selected]
            selected.append(random.choice(left))
        for select in selected: 
            context.append("visible(" + select + ")")
            closest_ring = random.choice(psms)
            closest_peg = random.choice(psms)
            context.append("distance(" + closest_ring + ",ring," + select + ",1)")
            context.append("distance(" + [psm for psm in psms if psm != closest_ring][0] + ",ring," + select + ",2)")
            context.append("distance(" + closest_peg + ",peg," + select + ",1)")
            context.append("distance(" + [psm for psm in psms if psm != closest_peg][0] + ",peg," + select + ",2)")
            if random.choice([0,1]) == 1:
                context.append("on(ring," + select + ",peg,grey)")

        context_list.append(context)
        for asp in asp_names:
            times = copy.deepcopy([])
            time_median = 0.
            out_file = path_pkg + "/../tests/" + asp
            if "text" in asp:
                for i in range(len(context)):
                    if "on(" in context[i]:
                        context[i] = context[i][:-1] + ",1)"
            else:
                context = context_list[-1]

            for _ in range(num_tests/5):
                solver = Solver(asp + ".lp", path_pkg)
                time, plan = solver.solve(context)
                times.append(time)

            time_median = np.median(times)
            if "text" in asp:
                time_list_text.append(time_median)
                plan_list_text.append(plan)
                time = np.array(time_list_text)
                plan = np.array(plan_list_text)
            else:
                time_list.append(time_median)
                plan_list.append(plan)
                time = np.array(time_list)
                plan = np.array(plan_list)
            
            np.savez(out_file, plan=plan, time=time, context=np.array(context_list)) #label=value
            






def read():
    #READ
    file  = np.load("peg_ring.npz")
    times = file["time"]
    plans = file["plan"]

    file = np.load("peg_ring_text.npz")
    times_text = file["time"]
    plans_text = file["plan"]

    #SORT
    sort_index = np.argsort([len(p) for p in plans])
    plans = plans[sort_index]
    times = times[sort_index]
    plans_text = plans_text[sort_index]
    times_text = times_text[sort_index]

    #PLOT RATIO
    plt.figure()
    plt.plot(np.linspace(1, 100, num=100), np.divide(times_text, times), linewidth=3)
    plt.grid(linewidth=3)
    # plt.tick_params(labelbottom=False)
    plt.xlabel("Configuration", fontsize=35)
    plt.ylabel("Ratio", fontsize=35)
    plt.xlim(1)
    plt.yticks(fontsize=35)
    plt.xticks(fontsize=35)
    plt.show()

    #PLOT TIME VS. PLAN LENGTH
    # #original
    # plans_unique, count_duplicate = np.unique([len(p) for p in plans], return_counts=True)
    # times_clustered = []
    # j = 0
    # for i in range(np.shape(count_duplicate)[0]):
    #     times_clustered.append((np.mean(times[j:j+count_duplicate[i]]), np.std(times[j:j+count_duplicate[i]])))
    #     j += count_duplicate[i]
    # #text
    # plans_text_unique, count_duplicate = np.unique([len(p) for p in plans_text], return_counts=True)
    # times_text_clustered = []
    # j = 0
    # for i in range(np.shape(count_duplicate)[0]):
    #     times_text_clustered.append((np.mean(times_text[j:j+count_duplicate[i]]), np.std(times_text[j:j+count_duplicate[i]])))
    #     j += count_duplicate[i]
    
    # plt.figure()
    # #original
    # plt.errorbar(plans_unique, [time[0] for time in times_clustered], yerr=[time[1] for time in times_clustered], fmt='-o', elinewidth=3, markeredgewidth=3, capsize=5, label="Hand-written")
    # #text
    # plt.errorbar(plans_text_unique, [time[0] for time in times_text_clustered], yerr=[time[1] for time in times_text_clustered], fmt='-o', elinewidth=3, markeredgewidth=3, capsize=5, color='g', label="Text")
    # plt.legend(loc="upper left", fontsize=35)
    # plt.xlabel("Plan length", fontsize=35)
    # plt.ylabel("Planning time [s]", fontsize=35)
    # plt.xlim(left=plans_text_unique[0])
    # plt.xticks(fontsize=35)
    # plt.yticks(fontsize=35)
    # plt.grid(linewidth=3)
    # plt.show()







if __name__ == '__main__':
    # test()
    read()
